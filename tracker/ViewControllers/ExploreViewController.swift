//
//  ExploreViewController.swift
//  tracker
//
//  Created by Abraham Calvillo on 3/15/20.
//  Copyright © 2020 AbrahamCalvillo. All rights reserved.
//

import UIKit
import Parse

fileprivate let GALLERY_SECTION: [Int] = [0, 2]
fileprivate let TEXT_SECTION: [Int] = [3]
fileprivate let LIST_SECTION: [Int] = [1, 4]
@available(iOS 13.0, *)
class ExploreViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    
    var articleClass = PFObject(className: "Explore")
    var articleArray = [PFObject]()
    
    
    lazy var collectionView: UICollectionView = {
        let collectionView: UICollectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: self.makeLayout())
        collectionView.backgroundColor = UIColor.white
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(Cell.self, forCellWithReuseIdentifier: "cell")
        collectionView.register(Featured.self, forCellWithReuseIdentifier: "featured")
        collectionView.register(Text.self, forCellWithReuseIdentifier: "text")
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        return collectionView
    }()


    override func viewDidLoad() {
        super.viewDidLoad()
        
//        for family: String in UIFont.familyNames
//               {
//                   print(family)
//                   for names: String in UIFont.fontNames(forFamilyName: family)
//                   {
//                       print("== \(names)")
//                   }
//               }
        queryStores()
        self.navigationController?.isNavigationBarHidden = true
        self.collectionView.isUserInteractionEnabled = true
        self.view.addSubview(self.collectionView)
        NSLayoutConstraint.activate([
            self.collectionView.topAnchor.constraint(equalTo: self.view.layoutMarginsGuide.topAnchor),
            self.collectionView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor),
            self.collectionView.leftAnchor.constraint(equalTo: self.view.leftAnchor),
            self.collectionView.rightAnchor.constraint(equalTo: self.view.rightAnchor)
        ])
    }

    func makeLayout() -> UICollectionViewLayout {
        let layout = UICollectionViewCompositionalLayout { (section: Int, environment: NSCollectionLayoutEnvironment) -> NSCollectionLayoutSection? in
            if GALLERY_SECTION.contains(section) {
                return LayoutBuilder.buildGallerySectionLayout(size: NSCollectionLayoutSize(widthDimension: .fractionalWidth(0.75), heightDimension: .fractionalHeight(0.5)))
            } else if TEXT_SECTION.contains(section) {
                return LayoutBuilder.buildTextSectionLayout()
            } else {
                return LayoutBuilder.buildTableSectionLayout()
            }
        }
        return layout
        
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if GALLERY_SECTION.contains(section) {
            return articleArray.count
        }
        if TEXT_SECTION.contains(section) {
            return 1
        }
        if LIST_SECTION.contains(section) {
            return 5
        }
        return 0
    }
    
    

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if GALLERY_SECTION.contains(indexPath.section) {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "featured", for: indexPath) as! Featured
            
            articleClass = articleArray[indexPath.row]
            cell.titleLabel.text = "\(articleClass["articleTitle"]!)"
            cell.subtitleLabel.text = "\(articleClass["artitcleText"]!)"
            let imageFile = articleClass["articleImage"] as? PFFileObject
            imageFile?.getDataInBackground { (imageData, error) -> Void in
                if error == nil {
                    if let imageData = imageData {
                        cell.viewBackground.image = UIImage(data:imageData)
                    }
                    
                }
                
            }
            
            return cell
        }
        if TEXT_SECTION.contains(indexPath.section) {
            return CellBuilder.getTextCell(collectionView: collectionView, indexPath: indexPath)
        }
        if LIST_SECTION.contains(indexPath.section) {
            return CellBuilder.getListCell(collectionView: collectionView, indexPath: indexPath)
        }
        return UICollectionViewCell()
    }
    
    
    func collectionView(_ collectionView:UICollectionView, didSelectItemAt indexPath: IndexPath){
        if GALLERY_SECTION.contains(indexPath.section){
            articleClass = articleArray[indexPath.row]
            //print("Selected Cell: \(articleClass["articleTitle"]!) in gallery section")
            let aVC = storyboard?.instantiateViewController(withIdentifier: "articleDetails") as! ArticleDetails
            aVC.articleObj = articleClass
            navigationController?.pushViewController(aVC, animated: true)
        }
        else{
            //print("Selected Cell: \(indexPath.row)")
            
            let appIndex = indexPath.row
            let app = TEXT_LIST[appIndex]
            
                
                
                
            let aVC = storyboard?.instantiateViewController(withIdentifier: "listDetails") as! ListDetails
            aVC.textHolder = app.article
            aVC.picHolder = app.pic
            aVC.authorHolder = app.author
            aVC.titleHolder = app.title
            navigationController?.pushViewController(aVC, animated: true)
           
          
            
        }
        
    }
    
    
   func queryStores() {
    let sv = UIViewController.displaySpinner(onView: self.view)
    let query = PFQuery(className: "Explore")
    if PFUser.current() != nil {
        query.findObjectsInBackground { (objects, error)-> Void in
            UIViewController.removeSpinner(spinner: sv)
            if error == nil{
                self.articleArray = objects!
                self.collectionView.reloadData()
            }
            else {
                print("\(error!.localizedDescription)")
            }}
    }
    
    }
}
