//
//  PeriodQuestionsViewController.swift
//  tracker
//
//  Created by Abraham Calvillo on 11/8/19.
//  Copyright © 2019 AbrahamCalvillo. All rights reserved.
//

import UIKit
import JTAppleCalendar




// This is the first view controller class that allows for a user to selected the date
//range of their period. Either from the current month of the previous month.
class PeriodQuestionsViewController: UIViewController {
    
    
    /*
     MARK: Variables
     */
    var firstD: Date!
    var lastD: Date!
    var diffOfDates: Int!
    var name = ""
    var nextInfo = [String]()
    var pic: UIImage!
    
    /*
     MARK: Outlets
     */
    @IBOutlet weak var numberBox: UILabel!
    @IBOutlet weak var monthLabel: UILabel!
    @IBOutlet weak var calendarView: JTACMonthView!
    
    
    var firstDate: Date?
    var twoDatesAlreadySelected: Bool {
        return firstDate != nil && calendarView.selectedDates.count > 1
    }
    
    /*
     MARK: viewDidLoad
     */
    override func viewDidLoad() {
        super.viewDidLoad()
        
        numberBox.setShadow(view: numberBox, radius: 15, color: pinkColor, shadowRadius: 12, offHeight: 15, opacity: 0.6)
        
        let date = Date()
        let nf = DateFormatter()
        nf.dateFormat = "MMMM yyyy"
        monthLabel.text = nf.string(from: date)
        calendarView.scrollingMode = .stopAtEachSection
        calendarView.allowsRangedSelection = true
        calendarView.allowsMultipleSelection = true
        
    }
    
    // function sets up the cells such as the dates, and also handles the text color of the cells
    // and calls the handleCellTextColor/handleCellSelected() functions to chane the color.
    func configureCell(view: JTACDayCell?, cellState: CellState) {
        guard let cell = view as? DataCell  else { return }
        cell.dateLabel.text = cellState.text
        handleCellTextColor(cell: cell, cellState: cellState)
        handleCellSelected(cell: cell, cellState: cellState)
    }
    
    
    
    
    // Hanlding the color and corner radius when a user selects a cell. Because of different
    // device sizes, some cells may appear with different spacing but consisent corner radius.
    func handleCellSelected(cell: DataCell, cellState: CellState) {
        cell.selectedView.isHidden = !cellState.isSelected
        if cellState.isSelected {
            cell.dateLabel.textColor = UIColor.white
        }
//        switch cellState.selectedPosition() {
//        case .left:
//            cell.selectedView.layer.cornerRadius = 20
//            cell.selectedView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner]
//        case .middle:
//            cell.selectedView.layer.cornerRadius = 20
//            cell.selectedView.layer.maskedCorners = []
//        case .right:
//            cell.selectedView.layer.cornerRadius = 20
//            cell.selectedView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMaxXMaxYCorner]
//        case .full:
//            cell.selectedView.layer.cornerRadius = 20
//            cell.selectedView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner, .layerMinXMaxYCorner, .layerMinXMinYCorner]
//        default: break
//        }
        
        cell.selectedView.layer.cornerRadius = cell.selectedView.frame.width / 2

    }
    
    
    func handleCellTextColor(cell: DataCell, cellState: CellState) {
        if cellState.dateBelongsTo == .thisMonth {
            cell.dateLabel.textColor = UIColor.black
            cell.isUserInteractionEnabled = true
            cell.isHidden = false
            
            
        } else {
            cell.dateLabel.textColor = UIColor.gray
            cell.isHidden = true
            cell.isUserInteractionEnabled = false
        }
    }
    
    
    // DRY. DRY. DRY. Need to move to configs file and make a constant so that I do not repeat
    // myself throughout the code. Left it here for now until I can refactor.
    func displayErrorMessage(message:String) {
        let alertView = UIAlertController(title: "Sorry!", message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
        }
        alertView.addAction(OKAction)
        if let presenter = alertView.popoverPresentationController {
            presenter.sourceView = self.view
            presenter.sourceRect = self.view.bounds
        }
        self.present(alertView, animated: true, completion:nil)
    }
    
    
    // Only way I could think of at the time to move information over to the next view. Do this
    // throught the application and will have to think of a better way down the road but for now
    // i send the users: name, previous information, profile img, and period dates to the next
    // view controller.
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "lastView"{
            let dest = segue.destination as! FlowLevelViewController
            
            dest.nameN = name
            dest.lastInfo.append(contentsOf: nextInfo)
            dest.profile = pic
            dest.dateDiff = diffOfDates
            dest.dates.append(contentsOf: sender as! [Date])
            // dest.info.append(contentsOf: sender as! [String])
        }
    }
    
    @IBAction func nextBtn(_ sender: AnyObject?){
        guard let dte = firstD, dte != nil, let sDte = lastD, sDte != nil else {
            // show an alert
            displayErrorMessage(message: "Please select dates.")
            
            
            return
        }
        performSegue(withIdentifier: "lastView", sender: [dte, sDte])
        
    }
    
}



extension PeriodQuestionsViewController: JTACMonthViewDataSource {
    
    
    func configureCalendar(_ calendar: JTACMonthView) -> ConfigurationParameters {
        
        
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy MM dd"
        formatter.timeZone = Calendar.current.timeZone
        formatter.locale = Calendar.current.locale
        
        var dateComponent = DateComponents()
        dateComponent.month = 1
        let startDate = Date()
        let endDate = Calendar.current.date(byAdding: dateComponent, to: startDate)
        
        let parameters = ConfigurationParameters(startDate: startDate, endDate: endDate!, numberOfRows: 6, calendar: Calendar.current, generateInDates: .forAllMonths , generateOutDates: .tillEndOfGrid , firstDayOfWeek: .sunday, hasStrictBoundaries: false)
        
        
        
        
        return parameters
    }
}


extension PeriodQuestionsViewController: JTACMonthViewDelegate {
    
    
    
    func calendar(_ calendar: JTACMonthView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTACDayCell {
        let cell = calendar.dequeueReusableJTAppleCell(withReuseIdentifier: "dateCell", for: indexPath) as! DataCell
        self.calendar(calendar, willDisplay: cell, forItemAt: date, cellState: cellState, indexPath: indexPath)
        
        
        return cell
    }
    
    func calendar(_ calendar: JTACMonthView, willDisplay cell: JTACDayCell, forItemAt date: Date, cellState: CellState, indexPath: IndexPath) {
        configureCell(view: cell, cellState: cellState)
        
        
        
        
    }
    
    func calendar(_ calendar: JTACMonthView, didSelectDate date: Date, cell: JTACDayCell?, cellState: CellState, indexPath: IndexPath) {
        //configureCell(view: cell, cellState: cellState)
        
        if firstDate != nil {
            calendar.selectDates(from: firstDate!, to: date,  triggerSelectionDelegate: false, keepSelectionIfMultiSelectionAllowed: true)
            
            self.firstD = firstDate!
            self.lastD = date
            self.diffOfDates = (daysBetween(start: firstDate!, end: date) + 1)
            
            
            
        } else {
            firstDate = date
        }
        configureCell(view: cell, cellState: cellState)
        
    }
    func daysBetween(start: Date, end: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: start, to: end).day!
    }
    
    
    func calendar(_ calendar: JTACMonthView, didDeselectDate date: Date, cell: JTACDayCell?, cellState: CellState, indexPath: IndexPath) {
        
        configureCell(view: cell, cellState: cellState)
        
        
        
        
    }
    
    func calendar(_ calendar: JTACMonthView, shouldSelectDate date: Date, cell: JTACDayCell?, cellState: CellState, indexPath: IndexPath) -> Bool {
        
        
        if twoDatesAlreadySelected && cellState.selectionType != .programatic || firstDate != nil && date < calendarView.selectedDates[0]{
            firstDate = nil
            let retval = !calendarView.selectedDates.contains(date)
            calendarView.deselectAllDates()
            
            return retval
        }
        
        return true
    }
    
    func calendar(_ calendar: JTACMonthView, shouldDeselectDate date: Date, cell: JTACDayCell?, cellState: CellState, indexPath: IndexPath) -> Bool {
        if twoDatesAlreadySelected && cellState.selectionType != .programatic || !twoDatesAlreadySelected && cellState.selectionType != .programatic{
            firstDate = nil
            calendarView.deselectAllDates()
            
            return false
        }
        return true
    }
    
    func calendar(_ calendar: JTACMonthView, didScrollToDateSegmentWith visibleDates: DateSegmentInfo) {
        //let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM yyyy"
        let firstDay = visibleDates.monthDates.first?.date
        monthLabel.text = dateFormatter.string(from: firstDay!)
    }
}
