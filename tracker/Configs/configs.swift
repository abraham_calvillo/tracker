//
//  configs.swift
//  tracker
//
//  Created by Abraham Calvillo on 9/12/19.
//  Copyright © 2019 AbrahamCalvillo. All rights reserved.
//

import Foundation
import UIKit


let lightBlueColor = UIColor(red:199/255, green: 225/255, blue:  255/255, alpha: 1.0)
let blueColor = UIColor(red: 127/255, green: 178/255, blue: 255/255, alpha: 1.0)
let pinkUnderline = UIColor(red: 224/255, green: 148/255, blue: 190/255, alpha: 1.0)
let pinkColor = UIColor(red: 224/255, green: 148/255, blue: 190/255, alpha: 1.0)
let yellowColor = UIColor(red: 255/255, green: 204/255, blue: 0/255, alpha: 1.0)
let maroonColor = UIColor(red: 128/255, green: 0/255, blue: 26/255, alpha: 1.0)
let purpleColor = UIColor(red:148/255,green: 2/255,blue: 255/255, alpha: 0.7)
let greenColor = UIColor(red:32/255,green: 94/255,blue: 71/255, alpha: 1.0)


var themeColor =  pinkColor

var profile: UIImage?


let phases = ["Menstruation",
              "Follicular phase",
              "Ovulation",
              "Luteal Phase"]



extension UIView{
    
    
    
    /*-------------------- For the record, I added functions just for specific use cases, probably not the best thing to do but ehh, oh well it works ---------------------*/
    
    
    /*
     *  This function is use to set the drop
     *  shadow and the corner radius size for
     * the image in the top left.
     */
    func setShadow(view: UIView, radius: Int, color: UIColor, shadowRadius: Int, offHeight: Int, opacity: Float){
        
        
        
        
        view.layer.cornerRadius = CGFloat(radius)
        view.layer.borderWidth = 1.0
        view.layer.shadowRadius = CGFloat(shadowRadius)
        view.layer.borderColor = UIColor.clear.cgColor
        view.layer.shadowColor = color.cgColor
        view.layer.shadowOffset = CGSize(width: 0, height: offHeight)
        
        
        let borderView = UIView()
        borderView.frame = view.bounds
        borderView.layer.cornerRadius = 10
        borderView.layer.borderColor = UIColor.clear.cgColor
        borderView.layer.borderWidth = 1.0
        borderView.layer.masksToBounds = true
        view.addSubview(borderView)
        
        view.layer.shadowOpacity = opacity
        view.layer.masksToBounds = false
        view.layer.shadowPath = UIBezierPath(roundedRect: view.bounds, cornerRadius: view.layer.cornerRadius).cgPath
        
        
        
    }
    
    
    /*
     *  This function is to set the color, drop
     *  shadow, and gradient for the big ass
     *  circle on the main view. Possibly going
     *  to let user change color in later
     *  iterations or give them suggested colors
     *  to choose from?
     */
    func setGradient(view: UIView, colorOne: UIColor, colorTwo: UIColor){
        
        
        let gradient = CAGradientLayer()
        view.layer.cornerRadius = view.frame.size.width/2
        view.layer.borderWidth = 1.0
        view.layer.shadowRadius = 12.0
        view.layer.borderColor = UIColor.clear.cgColor
        view.layer.shadowColor = colorTwo.cgColor//UIColor.black.cgColor
        view.layer.shadowOffset = CGSize(width: 15, height: 20)
        view.layer.shadowOpacity = 0.5
        view.layer.masksToBounds = false
        gradient.frame = view.bounds
        gradient.masksToBounds = false
        gradient.cornerRadius = view.frame.size.width/2
        gradient.masksToBounds = true
        gradient.colors = [colorOne.cgColor, colorTwo.cgColor]
        gradient.startPoint = CGPoint.zero
        gradient.endPoint = CGPoint(x: 1, y: 1)
        view.layer.insertSublayer(gradient, at: 0)
        view.layer.shadowPath = UIBezierPath(roundedRect: view.bounds, cornerRadius: view.layer.cornerRadius).cgPath
        
        
    }
    
    
}

extension UITextField {
    func setBottomBorder(color: UIColor, opacity: Float) {
        self.borderStyle = .none
        self.layer.backgroundColor = UIColor.white.cgColor
        
        self.layer.masksToBounds = false
        self.layer.shadowColor = color.cgColor//UIColor(red: 249/255, green: 128/255, blue: 243/255, alpha: 1).cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.layer.shadowOpacity = opacity
        self.layer.shadowRadius = 0.0
    }
}

extension UIViewController
{
    
    
    func hideKeyboard()
    {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(UIViewController.dismissKeyboard))
        
        tap.cancelsTouchesInView = false
        //view.resignFirstResponder()
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard()
    {
        view.endEditing(true)
    }
    
    class func displaySpinner(onView : UIView) -> UIView {
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = UIColor.init(red: 1, green: 1, blue: 1, alpha: 0.8)
        let ai = UIActivityIndicatorView.init(style: .whiteLarge)
        ai.color = .lightGray
        ai.startAnimating()
        ai.center = spinnerView.center
        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            onView.addSubview(spinnerView)
        }
        
        return spinnerView
    }
    
    class func removeSpinner(spinner :UIView) {
        DispatchQueue.main.async {
            spinner.removeFromSuperview()
        }
    }
    
    
    
}






// func loadHomeScreen(){
// let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
// let loggedInViewController = storyBoard.instantiateViewController(withIdentifier: "LoggedIn")
// self.present(loggedInViewController, animated: false, completion: nil)
// }
/*
 @IBAction func cancel(_ unwindSegue: UIStoryboardSegue) {}
 }
 
 */


//extension UIView {
//    @IBInspectable var cornerRadius: CGFloat {
//        get {
//            return layer.cornerRadius
//        }
//        set {
//            layer.cornerRadius = newValue
//            //layer.masksToBounds = newValue > 0
//        }
//    }
//}


func displayErrorMessage(message:String) {
    let alertWindow = UIWindow(frame: UIScreen.main.bounds)
    
    
    var view: UIView!
    let alertView = UIAlertController(title: "Error!", message: message, preferredStyle: .alert)
    let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
    }
    alertView.addAction(OKAction)
    if let presenter = alertView.popoverPresentationController {
        presenter.sourceView = view
        presenter.sourceRect = view.bounds
    }
    
    alertWindow.rootViewController?.present(alertView, animated: true, completion: nil)
    
    //present(alertView, animated: true, completion:nil)
}

extension Date {
    
    func totalDistance(from date: Date, resultIn component: Calendar.Component) -> Int? {
        return Calendar.current.dateComponents([component], from: self, to: date).value(for: component)
    }
    
    func compare(with date: Date, only component: Calendar.Component) -> Int {
        let days1 = Calendar.current.component(component, from: self)
        let days2 = Calendar.current.component(component, from: date)
        return days1 - days2
    }
    
    func hasSame(_ component: Calendar.Component, as date: Date) -> Bool {
        return self.compare(with: date, only: component) == 0
    }
}
