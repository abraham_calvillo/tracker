//
//  CalendarViewController.swift
//  tracker
//
//  Created by Abraham Calvillo on 2/28/20.
//  Copyright © 2020 AbrahamCalvillo. All rights reserved.
//

import UIKit
import JTAppleCalendar
import Parse

class CalendarViewController: UIViewController {

    //MARK: variables
    private let user = PFUser.current()!
    @IBOutlet weak var begCycleLabel: UILabel!
    @IBOutlet weak var endCycleLabel: UILabel!
    @IBOutlet weak var nextCycleLabel: UILabel!
    @IBOutlet weak var avgCycleLabel: UILabel!
    @IBOutlet weak var flowTypeLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var saveBtn: UIButton!
    
    @IBOutlet weak var calendarView: JTACMonthView!


    
    
    var firstDate: Date?
    var lastDate: Date?
    var twoDatesAlreadySelected: Bool {
        return firstDate != nil && calendarView.selectedDates.count > 1
    }
    
    //MARK: View Will Appear
    override func viewWillAppear(_ animated: Bool) {
        loadViews()
    }
    
    //MARK: View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        calendarView.allowsRangedSelection = true
        calendarView.allowsMultipleSelection = true
        loadViews()
        //setColors()
    }
    
    
    //MARK: Load View
    private func loadViews(){
        let monthFormatter = DateFormatter()
        let currentYearDate = DateFormatter()
        let date = Date()
        monthFormatter.dateFormat = "MMM. dd"
        currentYearDate.dateFormat = "MMMM yyyy"
        
        let beginning = user["cycleStart"] as? Date
        let end = user["cycleEnd"] as? Date
        let next = user["cycleTime"] as? Date
        let avg = user["avgDaysOfFlow"] as? Int
        let flow = user["flowType"] as? String
        
        
        dateLabel.text = currentYearDate.string(from: date)

        begCycleLabel.text = "\(monthFormatter.string(from: beginning!))"
        endCycleLabel.text = "\(monthFormatter.string(from: end!))"
        if(next == nil){
            nextCycleLabel.text = "-"
        }else{
            nextCycleLabel.text = "\(monthFormatter.string(from: next!))"
        }
        avgCycleLabel.text = "\(avg!)"
        
        
        if(flow == "nil"){
            flowTypeLabel.text = "-"
        }else{
            flowTypeLabel.text = flow
        }
        
        saveBtn.layer.cornerRadius = saveBtn.bounds.height / 2
        saveBtn.backgroundColor = themeColor

    }
    
    
    private func setColors(){
        if(user["themeColor"] as? String == "pinkColor")   {
            themeColor = pinkColor
            saveBtn.backgroundColor = themeColor
        }
        else if(user["themeColor"] as? String == "blueColor")   {
            themeColor = blueColor
            
            
        }
        else if(user["themeColor"] as? String == "marColor"){
            themeColor = maroonColor
            
        }
        else if(user["themeColor"] as? String == "purpColor"){
            themeColor = purpleColor
        
        }
        else if(user["themeColor"] as? String == "greenColor"){
            themeColor = greenColor
           
        }
    }
    
    // function sets up the cells such as the dates, and also handles the text color of the cells
       // and calls the handleCellTextColor/handleCellSelected() functions to chane the color.
       func configureCell(view: JTACDayCell?, cellState: CellState) {
           guard let cell = view as? DataCell  else { return }
           cell.dateLabel.text = cellState.text
           handleCellTextColor(cell: cell, cellState: cellState)
           handleCellSelected(cell: cell, cellState: cellState)
       }
       
       
       
       
       // Hanlding the color and corner radius when a user selects a cell. Because of different
       // device sizes, some cells may appear with different spacing but consisent corner radius.
       func handleCellSelected(cell: DataCell, cellState: CellState) {
           cell.selectedView.isHidden = !cellState.isSelected
           if cellState.isSelected {
            cell.dateLabel.textColor = UIColor.white
            cell.selectedView.backgroundColor = themeColor
           }
//           switch cellState.selectedPosition() {
//           case .left:
//               cell.selectedView.layer.cornerRadius = 20
//               cell.selectedView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner]
//           case .middle:
//               cell.selectedView.layer.cornerRadius = 20
//               cell.selectedView.layer.maskedCorners = []
//           case .right:
//               cell.selectedView.layer.cornerRadius = 20
//               cell.selectedView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMaxXMaxYCorner]
//           case .full:
//               cell.selectedView.layer.cornerRadius = 20
//               cell.selectedView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner, .layerMinXMaxYCorner, .layerMinXMinYCorner]
//           default: break
//           }
        
        cell.selectedView.layer.cornerRadius = cell.selectedView.frame.width / 2
       }
       
       
       func handleCellTextColor(cell: DataCell, cellState: CellState) {
           if cellState.dateBelongsTo == .thisMonth {
               cell.dateLabel.textColor = UIColor.black
               cell.isUserInteractionEnabled = true
               cell.isHidden = false
               
               
           } else {
               cell.dateLabel.textColor = UIColor.gray
               cell.isHidden = true
               cell.isUserInteractionEnabled = false
           }
       }
    
    @IBAction func newDates(_sender: AnyObject?){
        if twoDatesAlreadySelected{
           
            user["cycleStart"] = firstDate
            user["cycleEnd"]   = lastDate
            user.saveInBackground()
            displayMessage(message: "Your dates have been updated!")
        }
        else{
            
             displayErrorMessage(message: "You must select dates first.")
        }
    }
    
    func displayErrorMessage(message:String) {
        let alertView = UIAlertController(title: "Sorry!", message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
        }
        alertView.addAction(OKAction)
        if let presenter = alertView.popoverPresentationController {
            presenter.sourceView = self.view
            presenter.sourceRect = self.view.bounds
        }
        self.present(alertView, animated: true, completion:nil)
    }
    
    
    func displayMessage(message:String) {
        let alertView = UIAlertController(title: "All Done!", message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
        }
        alertView.addAction(OKAction)
        if let presenter = alertView.popoverPresentationController {
            presenter.sourceView = self.view
            presenter.sourceRect = self.view.bounds
        }
        self.present(alertView, animated: true, completion:nil)
    }
    


}
  
extension CalendarViewController: JTACMonthViewDataSource{
    
    func configureCalendar(_ calendar: JTACMonthView) -> ConfigurationParameters {
         
               let formatter = DateFormatter()
               formatter.dateFormat = "yyyy MM dd"
               formatter.timeZone = Calendar.current.timeZone
               formatter.locale = Calendar.current.locale
               
               var dateComponent = DateComponents()
               dateComponent.month = 1
               let startDate = Date()
               let endDate = Calendar.current.date(byAdding: dateComponent, to: startDate)
               
               let parameters = ConfigurationParameters(startDate: startDate, endDate: endDate!, numberOfRows: 6, calendar: Calendar.current, generateInDates: .forAllMonths , generateOutDates: .tillEndOfGrid , firstDayOfWeek: .sunday, hasStrictBoundaries: false)
               
               
               
               
               return parameters
    }
    
}

extension CalendarViewController:JTACMonthViewDelegate{
    
    
    func calendar(_ calendar: JTACMonthView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTACDayCell{
        
        let cell = calendar.dequeueReusableJTAppleCell(withReuseIdentifier: "dateCell", for: indexPath) as! DataCell
        self.calendar(calendar, willDisplay: cell, forItemAt: date, cellState: cellState, indexPath: indexPath)
        
        
        return cell
        
    }
    
    func calendar(_ calendar: JTACMonthView, willDisplay cell: JTACDayCell, forItemAt date:Date, cellState: CellState, indexPath: IndexPath){
        
        configureCell(view: cell, cellState: cellState)

          
      }
    
    func calendar(_ calendar: JTACMonthView, didSelectDate date: Date, cell: JTACDayCell?, cellState: CellState, indexPath: IndexPath) {
        
        
        if firstDate != nil {
            calendar.selectDates(from: firstDate!, to: date,  triggerSelectionDelegate: false, keepSelectionIfMultiSelectionAllowed: true)

            self.firstDate = firstDate!
            self.lastDate = date
//            self.diffOfDates = (daysBetween(start: firstDate!, end: date) + 1)
//
//
            
        } else {
            firstDate = date
        }
        configureCell(view: cell, cellState: cellState)
    }
    
    func daysBetween(start: Date, end: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: start, to: end).day!
    }
    
    func calendar(_ calendar: JTACMonthView, didDeselectDate date: Date, cell: JTACDayCell?, cellState: CellState, indexPath: IndexPath) {
        configureCell(view: cell, cellState: cellState)

    }
    
     func calendar(_ calendar: JTACMonthView, shouldSelectDate date: Date, cell: JTACDayCell?, cellState: CellState, indexPath: IndexPath) -> Bool {
        
        if twoDatesAlreadySelected && cellState.selectionType != .programatic || firstDate != nil && date < calendarView.selectedDates[0]{
                   firstDate = nil
                   let retval = !calendarView.selectedDates.contains(date)
                   calendarView.deselectAllDates()
                   
                   return retval
               }
               
               return true
    }
    
     func calendar(_ calendar: JTACMonthView, shouldDeselectDate date: Date, cell: JTACDayCell?, cellState: CellState, indexPath: IndexPath) -> Bool {
        if twoDatesAlreadySelected && cellState.selectionType != .programatic || !twoDatesAlreadySelected && cellState.selectionType != .programatic{
            firstDate = nil
            calendarView.deselectAllDates()
            
            return false
        }
        return true
    }
    
    func calendar(_ calendar: JTACMonthView, didScrollToDateSegmentWith visibleDates: DateSegmentInfo) {
        let dateFormatter = DateFormatter()
               dateFormatter.dateFormat = "MMMM yyyy"
               let firstDay = visibleDates.monthDates.first?.date
               dateLabel.text = dateFormatter.string(from: firstDay!)
    }
    
    
    
}

