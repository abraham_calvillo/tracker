//
//  CustomUIView.swift
//  tracker
//
//  Created by Abraham Calvillo on 9/13/19.
//  Copyright © 2019 AbrahamCalvillo. All rights reserved.
//

import UIKit

@IBDesignable
class CustomUIView: UIView {
    
    var bgColor = themeColor
    private var shapeLayer: CALayer?
    
    override func draw(_ rect: CGRect) {
        self.addShape(col: bgColor)
    }
    
  
    
    func addShape(col: UIColor){
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = createPathCircle()
        //shapeLayer.strokeColor = UIColor.lightGray.cgColor
        shapeLayer.fillColor = themeColor.cgColor
        
        shapeLayer.lineWidth = 1.0
        shapeLayer.opacity = 0.2
        
        if let oldShapeLayer = self.shapeLayer{
            self.layer.replaceSublayer(oldShapeLayer, with: shapeLayer)
        }else{
            self.layer.insertSublayer(shapeLayer, at: 0)
        }
        
        self.shapeLayer = shapeLayer
    }
    
    
    
    func createPathCircle() -> CGPath {
        
        let radius: CGFloat = self.frame.width + 100
        let path = UIBezierPath()
        let centerWidth = self.frame.width / 2
        
        path.move(to: CGPoint(x: 0, y: 0))
        path.addLine(to: CGPoint(x: (centerWidth - radius * 2), y: 0))
        path.addArc(withCenter: CGPoint(x: centerWidth, y: (self.frame.height  - 75)), radius: radius / 2, startAngle: CGFloat(180).degreesToRadians, endAngle: CGFloat(360).degreesToRadians, clockwise: false)
        path.addLine(to: CGPoint(x: self.frame.width, y: 0))
        path.addLine(to: CGPoint(x: self.frame.width, y: self.frame.height - 300))
        path.addLine(to: CGPoint(x: 0, y: self.frame.height - 300))
        path.close()
        return path.cgPath
    }
}

extension CGFloat {
    var degreesToRadians: CGFloat { return self * .pi / 180 }
    var radiansToDegrees: CGFloat { return self * 180 / .pi }
}
