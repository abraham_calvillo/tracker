//
//  Utils.swift
//  CompositionalLayoutDemo
//
//  Created by Kévin MAAREK on 26/09/2019.
//  Copyright © 2019 Kévin MAAREK. All rights reserved.
//

import UIKit


//let maroonColor = UIColor(red: 128/255, green: 0/255, blue: 26/255, alpha: 1.0)

public extension UIColor {
   convenience init(red: Int, green: Int, blue: Int) {
       assert(red >= 0 && red <= 255, "Invalid red component")
       assert(green >= 0 && green <= 255, "Invalid green component")
       assert(blue >= 0 && blue <= 255, "Invalid blue component")

       self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
   }

   convenience init(rgb: Int) {
       self.init(
           red: (rgb >> 16) & 0xFF,
           green: (rgb >> 8) & 0xFF,
           blue: rgb & 0xFF
       )
   }
}


public let APP_LIST: [(title: String, category:String, color: Int, pic: UIImage)] = [
    (title: "First Period?", category: "What to expect from your first period.", color: 0xfc0703,pic: UIImage(named: "padWwings")! ),
    (title: "Period myths", category: "Debunking common myths that people need to get straight.", color: 0x4263a6, pic: UIImage(named: "light")!),
    (title: "How to feel better on your period", category: "Some tips to help you feel better during your period.", color: 0xfc9403, pic: UIImage(named: "woman")!),
    (title: "Menopause", category: "What you should know about menopause.", color: 0x000000, pic: UIImage(named: "woman-head")!),
    (title: "Resoures", category: "Online resources for more information about your menstrual cycle and health.", color: 0xb30300, pic: UIImage(named: "aid")!)
]


func htmlFile(source: String)-> String{
    let path = Bundle.main.path(forResource: "../SupportingFiles/\(source)", ofType: "txt") // file path for file "data.txt"
    let string2 = try! String(contentsOfFile: path!, encoding: String.Encoding.utf8)
    return string2
}
public let TEXT_LIST: [(title: String, author:String, pic: UIImage, article: String)] = [
    (title: "What to Expect from Your First Period", author: "By Rachel Nall, RN, MSN, CRNA",pic: UIImage(named: "first.jpeg")!, article: htmlFile(source:"firstPeriod")),
    (title: "8 Period Myths We Need to Set Straight", author: "By Chaunie Brusie, BSN",pic: UIImage(named: "shocked.jpg")!, article: htmlFile(source: "8myths")),
    (title: "How to Feel Better on Your Period", author: "By Tampax",pic: UIImage(named: "happy.png")!, article: htmlFile(source: "feelBetter")),
    (title: "Menopause: 11 Things Every Woman Should Know", author: "By Josh Rotter and Natalie Silver",pic: UIImage(named: "mPause.jpg")!, article: htmlFile(source: "menopause")),
    (title: "Menstrual Cycle Resourses", author: "Full Moon",pic: UIImage(named: "power.jpg")!, article: htmlFile(source: "resources"))
    
]
