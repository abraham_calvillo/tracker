//
//  AccountViewController.swift
//  tracker
//
//  Created by Abraham Calvillo on 2/7/20.
//  Copyright © 2020 AbrahamCalvillo. All rights reserved.
//

import UIKit
import Parse
import YPImagePicker






class AccountViewController: UIViewController {
    
    @IBOutlet weak var signoutBtn: UIButton!
    @IBOutlet weak var pink: UIButton!
    @IBOutlet weak var maroon: UIButton!
    @IBOutlet weak var blue: UIButton!
    @IBOutlet weak var purple: UIButton!
    @IBOutlet weak var green: UIButton!
    @IBOutlet weak var uploadBtn: UIButton!
    @IBOutlet weak var holderView: UIView!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet fileprivate weak var userName: UITextField!
    @IBOutlet fileprivate weak var emailTextField: UITextField!
    @IBOutlet fileprivate weak var passwordTextfield: UITextField!
    weak var holderImage: PFFileObject!
    let bar = BubbleTabBar()
    weak var newPhoto: UIImage!
    weak var prof: UIImage!
    
    private let bgColor = themeColor
    private let user = PFUser.current()!
    private let delay = 1.0
    //private let group = DispatchGroup()
    
    
    //MARK: View will appear
    
    override func viewWillAppear(_ animated: Bool) {
        loadUsers()
        setupView()
    }
    
    
    
    /*
     MARK: viewDidLoad
     */
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        loadUsers()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        // setting the background colors of the buttons. move to its own function to make cleaner
        let btns = [pink, blue, maroon, purple,green]
        pink.backgroundColor = pinkColor
        blue.backgroundColor = blueColor
        maroon.backgroundColor = maroonColor
        purple.backgroundColor = purpleColor
        green.backgroundColor = greenColor
        
        
        // setting up button corner radius to make them round.
        for btn in btns{
            
            setupBtns(btn: btn!)
        }
        navigationController?.interactivePopGestureRecognizer?.delegate = self as? UIGestureRecognizerDelegate
        
        
        
        
        hideKeyboard()
        
    }
    
    
    
    //MARK: Load Users
    func loadUsers(){
        
        
        if let displayPhoto = PFUser.current()!["profileImg"] as? PFFileObject {
            displayPhoto.getDataInBackground { (imageData, error) -> Void in
                if error == nil {
                    if let imageData = imageData {
                        
                        
                        self.profileImage.image = UIImage(data: imageData)
                       
                    }
                    
                }
                
            }
            
        }
    }

    
    // MARK: Keyboard setting
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue{
            //self.view.frame.origin.y = 0
            
            
            let tabHeight = (self.tabBarController?.tabBar.frame.height)!
//            let minus = self.view.frame.origin.y - tabHeight//(sizer - 40)
//            if self.view.frame.origin.y == 0 {
//                self.view.frame.origin.y = minus//minus//keyboardSize.height //- 100
//            }
            
            let sizer = keyboardSize.height - 100
            let minus = self.view.frame.origin.y - (sizer - 70  + (tabHeight))
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y = minus//minus//keyboardSize.height //- 100
                
                
            }
            
        }
        
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        
        if self.view.frame.origin.y != 0 {
            
            self.view.frame.origin.y = 0
        }
    }
    
    /*
        MARK: - Load Log In
        */
       // When user is logged out, they will be redirected to the login screen. In iOS 13+ since
       // modals are presented differently, I am re-instantiating the intial view controller in
       // order to allow for swipe back features.
       func loadLoginScreen(){
           
           UserDefaults.standard.set(false, forKey: "isUserLoggedIn")
           UserDefaults.standard.synchronize()
           
           let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "NavControl") as! UINavigationController
           
           let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
           
           appDel.window?.rootViewController = loginVC
           
           //        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
           //        let viewController = storyBoard.instantiateViewController(withIdentifier: "loginScreen") as! SignUpViewController
           //        self.present(viewController, animated: true, completion: nil)
           
       }
       
       
       /*
        MARK: - Log Out
        */
       
       @IBAction func logOutBtn(){
           
           let sv = UIViewController.displaySpinner(onView: self.view)
           
           PFUser.logOutInBackground { (error: Error?) in
               UIViewController.removeSpinner(spinner: sv)
               if (error == nil){
                   self.loadLoginScreen()
                print("1")
                   
               }else{
                   if let descrip = error?.localizedDescription{
                       displayErrorMessage(message: descrip)
                   }else{
                       displayErrorMessage(message: "error logging out")
                   }
                   
               }
           }
       }
       
    
    // MARK: Image Selection
    // let user select a new profile image.
    @IBAction func uploadNewPhoto(sender: Any?){
        var config = YPImagePickerConfiguration()
        config.library.minNumberOfItems = 1
        config.library.maxNumberOfItems = 1
        config.library.onlySquare  = true
        config.library.mediaType = .photo
        config.startOnScreen = YPPickerScreen.photo
        config.screens = [.library]
        config.library.skipSelectionsGallery = false
        let picker = YPImagePicker(configuration: config)
        
        
        
            picker.didFinishPicking { [unowned picker] items, _ in
                if let photo = items.singlePhoto {
                   
                    self.profileImage.image = photo.image
                    self.newPhoto = photo.image
                    //print("dismissed for some reason")
                    
                   
                }
                picker.dismiss(animated: true, completion: nil)
                //picker.removeFromParent()

                if self.profileImage.image == nil{
                    
                    // do nothing
                }
                
            }
        
        picker.modalPresentationStyle = .overCurrentContext
        present(picker, animated: true, completion: nil)
    }
    
    //MARK: Set Btns
    // set up button corner radius
    func setupBtns(btn: UIButton){
        btn.layer.cornerRadius = btn.bounds.width / 2
        
    }
    
    //MARK: Set View
    // sets views on the view controller.
    func setupView(){
        
        signoutBtn.layer.opacity = 0.4
        
        if(user["themeColor"] as? String == "pinkColor")   {
            themeColor = pinkColor
            saveBtn.backgroundColor = pinkColor
            uploadBtn.backgroundColor = pinkColor
            self.signoutBtn.imageView!.tintColor = themeColor

        }
        else if(user["themeColor"] as? String == "blueColor")   {
            themeColor = blueColor
            uploadBtn.backgroundColor = blueColor

            saveBtn.backgroundColor = blueColor
            self.signoutBtn.imageView!.tintColor = themeColor
        }
        else if(user["themeColor"] as? String == "marColor"){
            themeColor = maroonColor
            saveBtn.backgroundColor = maroonColor
            uploadBtn.backgroundColor = maroonColor
            self.signoutBtn.imageView!.tintColor = themeColor

        }
        else if(user["themeColor"] as? String == "purpColor"){
            themeColor = purpleColor
            saveBtn.backgroundColor = purpleColor
            uploadBtn.backgroundColor = purpleColor
            self.signoutBtn.imageView!.tintColor = themeColor


        }
        else if(user["themeColor"] as? String == "greenColor"){
            themeColor = greenColor
            saveBtn.backgroundColor = greenColor
            uploadBtn.backgroundColor = greenColor
            self.signoutBtn.imageView!.tintColor = themeColor

        }
        userName.setBottomBorder(color: .lightGray, opacity: 0.7)
        emailTextField.setBottomBorder(color: .lightGray, opacity: 0.7)
        passwordTextfield.setBottomBorder(color: .lightGray, opacity: 0.7)
        saveBtn.tintColor = .white
//        saveBtn.backgroundColor = bgColor
        saveBtn.layer.cornerRadius = saveBtn.bounds.height / 2.0
        
        holderView.setShadow(view: holderView, radius: 25, color: themeColor, shadowRadius: 15, offHeight: 15, opacity: 0.34)
        
        profileImage.layer.cornerRadius = 25
        //profileImage.image = profile
        
        uploadBtn.layer.cornerRadius = uploadBtn.bounds.height / 2.0
        uploadBtn.backgroundColor = themeColor
        
    }
    
    @IBAction func pinkBtn(){
        
        themeColor = pinkColor
        changeBarItemColor(tint: pinkColor)
        user["themeColor"] = "pinkColor"
        
        user.saveInBackground()
        UIButton.animate(withDuration: delay){
            self.uploadBtn.backgroundColor = themeColor
            self.saveBtn.backgroundColor = themeColor
            self.signoutBtn.imageView!.tintColor = themeColor
        }
        UIView.animate(withDuration: delay){
            self.holderView.layer.shadowColor = themeColor.cgColor
        }
    }
    
    // MARK: Buttons
    @IBAction func blueBtn(){
        
        themeColor = blueColor
        changeBarItemColor(tint: blueColor)
        user["themeColor"] = "blueColor"
        user.saveInBackground()
        UIButton.animate(withDuration: delay){
            self.uploadBtn.backgroundColor = themeColor
            self.saveBtn.backgroundColor = themeColor
            self.signoutBtn.imageView!.tintColor = themeColor
        }
        UIView.animate(withDuration: delay){
            self.holderView.layer.shadowColor = themeColor.cgColor
        }
        
        
    }
    @IBAction func marBtn(){
        themeColor = maroonColor
        changeBarItemColor(tint: maroonColor)
        user["themeColor"] = "marColor"
        user.saveInBackground()
        UIButton.animate(withDuration: delay){
            self.uploadBtn.backgroundColor = themeColor
            self.saveBtn.backgroundColor = themeColor
            self.signoutBtn.imageView!.tintColor = themeColor

        }
        UIView.animate(withDuration: delay){
            self.holderView.layer.shadowColor = themeColor.cgColor
        }
    }
    
    @IBAction func purpBtn(){
        themeColor = purpleColor
        changeBarItemColor(tint: purpleColor)
        user["themeColor"] = "purpColor"
        user.saveInBackground()
        UIButton.animate(withDuration: delay){
            self.uploadBtn.backgroundColor = themeColor
            self.saveBtn.backgroundColor = themeColor
            self.signoutBtn.imageView!.tintColor = themeColor
        }
        UIView.animate(withDuration: delay){
            self.holderView.layer.shadowColor = themeColor.cgColor
        }
    }
    
    @IBAction func greenBtn(){
        
        themeColor = greenColor
        changeBarItemColor(tint: greenColor)
        user["themeColor"] = "greenColor"
        user.saveInBackground()
        UIButton.animate(withDuration: delay){
            self.uploadBtn.backgroundColor = themeColor
            self.saveBtn.backgroundColor = themeColor
            self.signoutBtn.imageView!.tintColor = themeColor

        }
        UIView.animate(withDuration: delay){
            self.holderView.layer.shadowColor = themeColor.cgColor
        }
        
//        let h = BubbleTabBar()
//        h.configure()
        
        
        
        
    }
    
    private func changeBarItemColor(tint: UIColor){
        guard let currentView = (UIApplication.shared.delegate as? AppDelegate)?.window?.rootViewController?.view,
        let superview = currentView.superview else { return }

        BubbleTabBar.appearance().tintColor = tint

        currentView.removeFromSuperview()
        UIView.animate(withDuration: 0.5){
            superview.addSubview(currentView)
        }
    }
    
    // MARK: Save Button
    @IBAction func saveChanges(){
        let userToUpdate = PFUser.current()!
        
        // Save Avatar
        if self.newPhoto != nil {
            let imageData = self.newPhoto.jpegData(compressionQuality: 0.5)
            let imageFile = PFFileObject(name:"avatar.jpg", data:imageData!)
            userToUpdate["profileImg"] = imageFile
        }
        
        
        if !(self.emailTextField.text!.isEmpty){
            userToUpdate["email"] = self.emailTextField.text!
            userToUpdate.username = self.emailTextField.text!
        }
        else{
        }
        
        if !(self.passwordTextfield.text!.isEmpty) {
            userToUpdate["password"] = self.passwordTextfield.text!
        }
        else{}
        
        if !(self.userName.text!.isEmpty) {
            userToUpdate["usersName"] = self.userName.text!
        }
        else{}
        let sv = UIViewController.displaySpinner(onView: self.view)
        userToUpdate.saveInBackground(block: { (succ, error) in
            UIViewController.removeSpinner(spinner: sv)
            
            if error == nil {
                print("Your Profile has been updated!")
            } else {
                print("Failed")
                
            }})
        
    }
    
    
    
    
    
    
    
}





