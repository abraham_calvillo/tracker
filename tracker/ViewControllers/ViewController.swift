//
//  ViewController.swift
//  tracker
//
//  Created by Abraham Calvillo on 9/12/19.
//  Copyright © 2019 AbrahamCalvillo. All rights reserved.
//

import UIKit
import Parse
import MBCircularProgressBar


class ViewController: UIViewController{
    
    
    
    @IBOutlet weak var acct: UIButton!
    @IBOutlet weak var ordView: CustomUIView!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var logOut: UIButton!
    @IBOutlet weak var holder: UIView!
    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var chanceLabel: UILabel!
    @IBOutlet weak var nextCycleLabel: UILabel!
    @IBOutlet weak var phasesLabel: UILabel!
    @IBOutlet weak var circleView: UIView!
    @IBOutlet weak var circleProgressBar: MBCircularProgressBarView!
    @IBOutlet weak var daysBox: UILabel!
    @IBOutlet weak var currentPhase: UILabel!
    @IBOutlet weak var morningLabel: UILabel!
    @IBOutlet weak var menu: UIView!
    
    @IBOutlet weak var clockItem: UIImageView!
    @IBOutlet weak var babyItem: UIImageView!
    @IBOutlet weak var uterusitem: UIImageView!

    
    
    var objColor = themeColor
    var changedColor: UIColor!
    var window: UIWindow?
    
    var storeObj = PFObject(className: "User")
    let gradient = CAGradientLayer()
    let user = PFUser.current()
    weak var vc: CustomUIView!
    
    
    
    
    /*
     MARK: View WillAppear
     */
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = true

        uterusitem.image = uterusitem.image?.withRenderingMode(.alwaysTemplate)
        babyItem.image = babyItem.image?.withRenderingMode(.alwaysTemplate)
        clockItem.image = clockItem.image?.withRenderingMode(.alwaysTemplate)
        loadUsers()
        objColor = themeColor
        
        if changedColor != objColor{
            UIView.animate(withDuration: 1){
                
                self.holder.layer.shadowColor = themeColor.cgColor
                self.circleProgressBar.layer.shadowColor = themeColor.cgColor
                self.babyItem.tintColor = themeColor
                self.clockItem.tintColor = themeColor
                self.uterusitem.tintColor = themeColor
                
            }
            
        }
        
        ordView.addShape(col: objColor)
        
        
        changedColor = objColor
        
        
        
    }
    
    
    
    
    /*
     MARK: - View DidLoad
     
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
       
        
        
        
        self.navigationController?.isNavigationBarHidden = true
        
        setupView()
        
        
        let today = Calendar.current.startOfDay(for: Date())
        var dateComps = DateComponents()
        dateComps.day = (PFUser.current()!["avgDaysOfFlow"] as? Int)!
        let cal = Calendar.current
        
        
        if PFUser.current()!["cycleTime"]  != nil {
            
            let cTime = (PFUser.current()!["cycleTime"] as? Date)!
            let lastDate = (PFUser.current()!["cycleEnd"] as? Date)!
            var dateComp = DateComponents()
            dateComp.month = 0
            var dateComponent = DateComponents()
            let calendar = Calendar.current
            let interval = calendar.dateInterval(of: .month, for: today)!
            
            var dayComp = DateComponents()
            dayComp.day = abs(30)
            
            // change year it will no of days in a year , change it to month it will give no of days in
            // a current month
            
            // Compute difference in days:
            let days = calendar.dateComponents([.day], from: interval.start, to: interval.end).day!
            
            let nextCycle = abs(daysBetween(start: today, end: lastDate))
            dateComponent.day = (days - nextCycle)
            let endDate = Calendar.current.date(byAdding: dayComp, to: cTime)
            
            
            let firstDate = (PFUser.current()!["cycleTime"] as? Date)!
            let since = cal.date(byAdding: dateComps, to: firstDate)!
            //            print("next start: \(firstDate)")
            //            print("next end: \(since)")
            //            print("next month: \(endDate!)")
            if today == firstDate{
                PFUser.current()!["cycleStart"] = firstDate
                PFUser.current()!["cycleEnd"] = since
                PFUser.current()!["cycleTime"] = endDate
                PFUser.current()!.saveInBackground()
                calcCycle()
            }
            else {
                calcCycle()
            }
            loadUsers()
        }
        else
        {
            calcCycle()
            loadUsers()
        }
        
    }
    
    
    
    
    
    // Calculates the days between the dates provided
    func daysBetween(start: Date, end: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: start, to: end).day!
    }
    
    
    
    /*
     MARK:- Setup View
     */
    
    func setupView(){
        
        uterusitem.image = uterusitem.image?.withRenderingMode(.alwaysTemplate)
        babyItem.image = babyItem.image?.withRenderingMode(.alwaysTemplate)
        clockItem.image = clockItem.image?.withRenderingMode(.alwaysTemplate)
        
        // sets the theme color on first initialization. perferably do this another way but leaving in for now until a I have time to fix
        if(user!["themeColor"] as? String == "pinkColor")   {
            themeColor = pinkColor
            self.babyItem.tintColor = pinkColor
            self.clockItem.tintColor = pinkColor
            self.uterusitem.tintColor = pinkColor
        }
        else if(user!["themeColor"] as? String == "blueColor")   {
            themeColor = blueColor
            self.babyItem.tintColor = blueColor
            self.clockItem.tintColor = blueColor
            self.uterusitem.tintColor = blueColor
            
        }
        else if(user!["themeColor"] as? String == "marColor"){
            themeColor = maroonColor
            self.babyItem.tintColor = maroonColor
            self.clockItem.tintColor = maroonColor
            self.uterusitem.tintColor = maroonColor
        }
        else if(user!["themeColor"] as? String == "purpColor"){
            themeColor = purpleColor
            self.babyItem.tintColor = purpleColor
            self.clockItem.tintColor = purpleColor
            self.uterusitem.tintColor = purpleColor
        }
        else if(user!["themeColor"] as? String == "greenColor"){
            themeColor = greenColor
            self.babyItem.tintColor = greenColor
            self.clockItem.tintColor = greenColor
            self.uterusitem.tintColor = greenColor
        }
        
        
        
        
        
        let hour = Calendar.current.component(.hour, from: Date())
        
        switch hour {
        case 6..<12 : morningLabel.text = "Good Morning,"
        case 12..<17 : morningLabel.text = "Good Afternoon,"
        case 17..<22 : morningLabel.text = "Good Evening,"
        default: morningLabel.text = "Good Night,"
        }
        
        
        
        holder.setShadow(view: holder, radius: 12, color: objColor, shadowRadius: 15, offHeight: 15, opacity: 0.8)
        shadowView.layer.cornerRadius = shadowView.bounds.width / 2
        shadowView.setShadow(view: shadowView, radius: Int(shadowView.bounds.width / 2), color: .white, shadowRadius: 20, offHeight: -15, opacity: 1.0)
        
        circleProgressBar.setShadow(view: circleProgressBar, radius: Int(circleProgressBar.bounds.width / 2) , color: objColor, shadowRadius: 20, offHeight: 35, opacity: 0.23)
        circleProgressBar.layer.cornerRadius = circleProgressBar.bounds.width / 2
        circleProgressBar.progressColor = yellowColor
        circleProgressBar.progressStrokeColor = yellowColor
        
    }
    
    
    
    
    
    
    /*
     MARK: - Calc Cycle
     */
    func calcCycle(){
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy MM dd"
        formatter.timeZone = Calendar.current.timeZone
        formatter.locale = Calendar.current.locale
        let today = Calendar.current.startOfDay(for: Date())
        
        let firstDate = (PFUser.current()!["cycleStart"] as? Date)!
        let lastDate = (PFUser.current()!["cycleEnd"] as? Date)!
        let calendar = Calendar.current
        let interval = calendar.dateInterval(of: .month, for: today)!
        let avgDays = PFUser.current()!["avgDaysOfFlow"] as! Int
        
        var dateComp = DateComponents()
        dateComp.month = 0
        let cal = Calendar.current
        let since = cal.date(from: dateComp)!
        let inter = cal.range(of: .day, in: .month, for: since)!
        
        let monthFormatter = DateFormatter()
        monthFormatter.dateFormat = "MMM. dd"
        
        let cycleForm = DateFormatter()
        cycleForm.dateFormat = "yyy MM dd"
        
        var dateComponent = DateComponents()
        
        
        // change year it will no of days in a year , change it to month it will give no of days in
        // a current month
        
        // Compute difference in days:
        let days = calendar.dateComponents([.day], from: interval.start, to: interval.end).day!
        circleProgressBar.maxValue = CGFloat(days)
        
        
        // MARK: Cycle Section
        
        /*
         // If todays date is larger than the first date and the last date of the period
         // meaning that today comes after the saved period is over.
         */
        if today > firstDate && today > lastDate {
            
            let nextCycle = abs(daysBetween(start: today, end: firstDate))
            dateComponent.day = (days - nextCycle)
            let endDate = Calendar.current.date(byAdding: dateComponent, to: today)
            //let holdPlease = (days - abs(daysBetween(start: today, end: lastDate) ))
            
            
            phasesLabel.text = "\(monthFormatter.string(from: endDate!))"
            
//            print(firstDate)
//            print(lastDate)
//            print((abs(daysBetween(start:firstDate, end: today))))
            
            daysBox.text = "\(abs(daysBetween(start:firstDate, end: today) ))"
            nextCycleLabel.text = "\(days - abs(daysBetween(start: today, end: firstDate) )) " + " days"
            //            print("This: \(daysBetween(start:firstDate, end: today) - holdPlease  + 1) ")
            circleProgressBar.value = CGFloat(abs(daysBetween(start:firstDate, end: today)  - 1))
            
            PFUser.current()!["cycleTime"] = endDate!
            PFUser.current()!.saveInBackground()
            
            
        }
            
            /*
             // During period
             */
        else if today <= lastDate && today >= firstDate{
            //dateComponent.day = abs(30)
            let nextCycle = abs(daysBetween(start: today, end: firstDate))
            dateComponent.day = (days - nextCycle)
            let endDate = Calendar.current.date(byAdding: dateComponent, to: today)
            //let holdPlease = (days - abs(daysBetween(start: today, end: lastDate) ))
            
            print(endDate!)
            phasesLabel.text = "\(monthFormatter.string(from: endDate!))"

            //let endDate = Calendar.current.date(byAdding: dateComponent, to: firstDate)
            currentPhase.text = phases[0]
            nextCycleLabel.text = "Current"
            //chanceLabel.text = "Low"
            daysBox.text = "\(abs(daysBetween(start: firstDate, end: today)) + 1 )"
            circleProgressBar.value = CGFloat(abs(daysBetween(start: firstDate, end: today)) + 1)
            phasesLabel.text = "\(monthFormatter.string(from: endDate!))"
            
            PFUser.current()!["cycleTime"] = endDate!
            PFUser.current()!.saveInBackground()
            
            
        }
            /*
             // If todays date is smaller than the first date and the last date of the period
             // meaning that today comes before the saved period starts.
             */
        else if today < firstDate && today < lastDate{
            
           // let lastCycle = abs(daysBetween(start: today, end: firstDate))
            dateComponent.day = (-days)
            let endCycle = Calendar.current.date(byAdding: dateComponent, to: firstDate)
            dateComponent.day = avgDays
            let e = Calendar.current.date(byAdding: dateComponent, to: endCycle!)
            //let holdPlease = (days - abs(daysBetween(start: today, end: lastDate) ))
            
            
            phasesLabel.text = "\(monthFormatter.string(from: firstDate))"
            
            let daysHolder = abs(daysBetween(start: today, end: firstDate))
            nextCycleLabel.text = "\(daysHolder) " + " days"
            daysBox.text = "\(abs(inter.count - (daysHolder)))"
            circleProgressBar.value = CGFloat(abs(inter.count - (daysHolder)))
            
            let nextCycle = abs(daysBetween(start: today, end: firstDate))
            dateComponent.day = nextCycle
            let endDate = Calendar.current.date(byAdding: dateComponent, to: today)
            
            PFUser.current()!["cycleStart"] = endCycle
            PFUser.current()!["cycleEnd"]   = e
            PFUser.current()!.saveInBackground()
           // phasesLabel.text = "\(monthFormatter.string(from: endDate!))"
            
        }
        
        
        /*
         let daysHolder = abs(daysBetween(start: firstDate, end: today)) + 1
         */
        
        let daysHolder = abs(daysBetween(start: today, end: firstDate) ) + 1
        let calcDate = daysHolder //abs(daysBetween(start:firstDate, end: today))
        //print(calcDate)
        switch calcDate{
        case 0..<avgDays :
            currentPhase.text = phases[0]
            chanceLabel.text = "Low"
        case avgDays..<15  :
            currentPhase.text = phases[1]
            chanceLabel.text = "Possible"
        case 15...16 :
            currentPhase.text = phases[2]
            chanceLabel.text = "High"
        case 16...days :
            
            currentPhase.text = phases[3]
            chanceLabel.text = "Possible"
        default : currentPhase.text = "none"
        }
        
    }
    
    
    // MARK: - Username and image
    // This function loads the username and the profile image for the user.
    func loadUsers(){
        
        if let displayName = PFUser.current()!["usersName"] as? String {
            userName.text = displayName + "!"
        }
        if let displayPhoto = PFUser.current()!["profileImg"] as? PFFileObject {
            displayPhoto.getDataInBackground { (imageData, error) -> Void in
                if error == nil {
                    if let imageData = imageData {
                        
                        
                        self.profileImg.image = UIImage(data:imageData)
                        //profile = UIImage(data:imageData)
                        
                    }
                    
                }
                
            }
            
        }
    }
    
    // preparing the account menu for segue by sending the profile image to be displayed. Doing this in
    // order to not create another parse object to pull the image from the server thus creating another unncecessary request.
    /*
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     if segue.identifier == "account"{
     let dest = segue.destination as! AccountViewController
     dest.prof = self.profileImg.image
     
     }
     }
     */
    
    //
    // Creating a segue into the account menu
    /*
     @IBAction func accountBtn(){
     performSegue(withIdentifier: "account", sender: nil)
     }
     */
}

