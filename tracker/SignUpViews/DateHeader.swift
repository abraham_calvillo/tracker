//
//  DateHeader.swift
//  tracker
//
//  Created by Abraham Calvillo on 1/29/20.
//  Copyright © 2020 AbrahamCalvillo. All rights reserved.
//

import Foundation
import UIKit
import JTAppleCalendar

class DateHeader: JTACMonthReusableView{
    
    @IBOutlet var monthTitle: UILabel!
    
}
