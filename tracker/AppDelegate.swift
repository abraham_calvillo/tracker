//
//  AppDelegate.swift
//  tracker
//
//  Created by Abraham Calvillo on 9/12/19.
//  Copyright © 2019 AbrahamCalvillo. All rights reserved.
//

import UIKit
import Parse
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        // AppDelegate.swift
        
        
                
        
        let parseConfig = ParseClientConfiguration {
            $0.applicationId = "ti2iYw5WxX7MCxZVIupGOO33xxk0eRZN1OTYBigV"
            $0.clientKey = "W9bq3S3IYx8lr8lGLL8ErqLujeOFRaQ0AUfpz9XI"
            $0.server = "https://parseapi.back4app.com"
        }
        
        Parse.initialize(with: parseConfig)
        
        
        
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
         (granted, error) in
         //print("Permission granted: \(granted)")
         guard granted else { return }
         self.getNotificationSettings()
     }
        
        
        /*
         
         let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
         if PFUser.current() != nil { // currentUser is not nil
         // set up app for valid in user
         
         //
         //            self.window?.rootViewController = storyboard.instantiateViewController(withIdentifier: "Home")
         let myController1 = storyboard.instantiateViewController(withIdentifier: "Home")
         let navC1 = UINavigationController(rootViewController: myController1)
         
         navC1.isNavigationBarHidden = true
         navC1.modalTransitionStyle = .coverVertical
         myController1.modalTransitionStyle = .coverVertical
         let user = PFUser.current()
         
         if(user!["themeColor"] as? String == "pinkColor")   {
         themeColor = pinkColor
         }
         else if(user!["themeColor"] as? String == "blueColor")   {
         themeColor = blueColor
         
         }
         else if(user!["themeColor"] as? String == "maroonColor"){
         themeColor = maroonColor
         }
         else if(user!["themeColor"] as? String == "purpColor"){
         themeColor = purpleColor
         }
         else if(user!["themeColor"] as? String == "greenColor"){
         themeColor = greenColor
         }
         
         // set the window's root view controller to be your new navigation controller
         self.window?.rootViewController = navC1
         
         return true // don't load the rootViewController from your storyboard (if you have an initial view set)
         } else { // there is no current user
         // set up app for new or non logged in user
         self.window?.rootViewController?.modalTransitionStyle = .coverVertical
         self.window?.rootViewController = storyboard.instantiateViewController(withIdentifier: "NavControl")
         
         
         //            let myController = storyboard.instantiateViewController(withIdentifier: "NavControl")
         //            let navC = UINavigationController(rootViewController: myController)
         //
         //            // set the window's root view controller to be your new navigation controller
         //            self.window?.rootViewController = navC
         //
         
         return true
         
         }*/
        
        return true
        
        
        
    }
    
    
    func getNotificationSettings() {
     UNUserNotificationCenter.current().getNotificationSettings { (settings) in
         //print("Notification settings: \(settings)")
         guard settings.authorizationStatus == .authorized else { return }
         DispatchQueue.main.async {
         UIApplication.shared.registerForRemoteNotifications()
        }
     }
 }

 func application(_ application: UIApplication,
                  didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
     createInstallationOnParse(deviceTokenData: deviceToken)
 }

 func application(_ application: UIApplication,
                  didFailToRegisterForRemoteNotificationsWithError error: Error) {
     print("Failed to register: \(error)")
 }

 func createInstallationOnParse(deviceTokenData:Data){
     if let installation = PFInstallation.current(){
         installation.setDeviceTokenFrom(deviceTokenData)
         installation.saveInBackground {
             (success: Bool, error: Error?) in
             if (success) {
                 print("You have successfully saved your push installation to Back4App!")
             } else {
                 if let myError = error{
                     print("Error saving parse installation \(myError.localizedDescription)")
                 }else{
                     print("Uknown error")
                 }
             }
         }
     }
 }
    
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
}

