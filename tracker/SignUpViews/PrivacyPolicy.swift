//
//  PrivacyPolicy.swift
//  tracker
//
//  Created by Abraham Calvillo on 1/31/20.
//  Copyright © 2020 AbrahamCalvillo. All rights reserved.
//

import Foundation
import WebKit
import UIKit



class PrivacyPolicy: UIViewController, WKNavigationDelegate{
    
    var webView: WKWebView!
    
    
    override func loadView() {
        webView = WKWebView()
        webView.navigationDelegate = self
        view = webView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let url = URL(string: "https://app.termly.io/document/privacy-policy/5c9298e4-9938-4652-b16f-8480a4810695")!
        webView.load(URLRequest(url: url))
        //webView.allowsBackForwardNavigationGestures = true
        

    }
    
    
    
    
}
