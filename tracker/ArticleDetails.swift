//
//  ArticleDetails.swift
//  tracker
//
//  Created by Abraham Calvillo on 3/16/20.
//  Copyright © 2020 AbrahamCalvillo. All rights reserved.
//

import UIKit
import Parse

class ArticleDetails: UIViewController {

    @IBOutlet weak var containerScrollView: UIScrollView!
    var articleObj = PFObject(className: "Explore")
    @IBOutlet weak var articleImg: UIImageView!
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var articleTitle: UILabel!
    @IBOutlet weak var line: UIView!
    
    
    @IBOutlet weak var articleText: UITextView!
    
    @IBOutlet weak var bottomView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true

navigationController?.interactivePopGestureRecognizer?.delegate = self as? UIGestureRecognizerDelegate
        setupView()
        // Do any additional setup after loading the view.
    }
    
    
    func setupView(){
        
        let aT = "\(articleObj["fullArticle"]!)"
        authorLabel.text = "\(articleObj["articleAuthor"]!)"
        articleTitle.text = "\(articleObj["articleTitle"]!)"
        articleText.setHTMLFromString(htmlText: aT)
        articleTitle.font = UIFont(name: "SofiaPro-Medium", size: 25)

        articleText.sizeToFit()
        articleTitle.sizeToFit()
        getImages()
        

        bottomView.frame.origin.y = articleText.frame.size.height + articleText.frame.origin.y
        containerScrollView.contentSize = CGSize(width: containerScrollView.frame.size.width,
        height: articleText.frame.size.height + articleText.frame.origin.y)
        
        
        
        
    }

    func getImages(){
        let imageFile = articleObj["articleImage"] as? PFFileObject
        imageFile?.getDataInBackground(block: { (imageData, error) -> Void in
            if error == nil {
                if let imageData = imageData {
                    self.articleImg.image = UIImage(data:imageData)
                    self.articleImg.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: UIImage(data: imageData)!.size.height)
                    self.articleImg.sizeToFit()
                    self.authorLabel.frame.origin.y = self.articleImg.frame.size.height + self.articleImg.frame.origin.y
                    self.line.frame.origin.y = self.authorLabel.frame.size.height + self.authorLabel.frame.origin.y
                    self.articleTitle.frame.origin.y = self.line.frame.size.height + self.line.frame.origin.y + 10
                    self.articleText.frame.origin.y = self.articleTitle.frame.size.height + self.articleTitle.frame.origin.y
                    
                    self.containerScrollView.contentSize = CGSize(width: self.containerScrollView.frame.size.width,
                                                                  height: self.articleText.frame.size.height + self.articleText.frame.origin.y)
                }
                
            }
            
        }
        )
        
        
    }
}


extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}

extension UITextView {
    func setHTMLFromString(htmlText: String) {
        let modifiedFont = String(format:"<span style=\"font-family:'-apple-system', 'HelveticaNeue'; font-size: \(16)\">%@</span>", htmlText)

        let attrStr = try! NSAttributedString(
            data: modifiedFont.data(using: .unicode, allowLossyConversion: true)!,
            options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue],
            documentAttributes: nil)
//        let style = NSMutableParagraphStyle()
//        style.lineSpacing = 50
//        let attributes = [NSAttributedString.Key.paragraphStyle: style]
//        self.attributedText = NSAttributedString(string: modifiedFont, attributes: attributes)

        self.attributedText = attrStr
    }
}
