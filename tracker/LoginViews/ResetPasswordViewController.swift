//
//  ResetPasswordViewController.swift
//  tracker
//
//  Created by Abraham Calvillo on 2/2/20.
//  Copyright © 2020 AbrahamCalvillo. All rights reserved.
//

import UIKit
import Parse
class ResetPasswordViewController: UIViewController {

    
    @IBOutlet weak var emailField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        
        emailField.text = ""
        emailField.delegate = self as? UITextFieldDelegate
        hideKeyboard()
    }
    
    
    @IBAction func resetPasswordBtn(){
//        PFUser.requestPasswordResetForEmail(inBackground: emailField.text!)
        
        PFUser.requestPasswordResetForEmail(inBackground: emailField.text!, block: { (succ, error) in
        if error == nil {
            
           
            self.simpleAlert("You will receive an email shortly with a link to reset your password")
            self.dismiss(animated: true, completion: nil)
        }})
        
        
    }
    

    func simpleAlert(_ mess:String) {
        let alert = UIAlertController(title: "Full Moon", message: mess, preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default, handler: { (action) -> Void in
            self.navigationController?.popViewController(animated: true)
        })
        alert.addAction(ok)
        present(alert, animated: true, completion: nil)
    }
    

}
