//
//  peronalInfoViewController.swift
//  tracker
//
//  Created by Abraham Calvillo on 11/7/19.
//  Copyright © 2019 AbrahamCalvillo. All rights reserved.
//

import UIKit
import YPImagePicker




class PeronalInfoViewController: UIViewController, UITextFieldDelegate {
  
    
    @IBOutlet weak var uploadBtn: UIButton!
    @IBOutlet weak var profilePicture: UIImageView!
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var numberBox: UIView!
    
   var info = [String]()
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
        
        profilePicture.image = UIImage(named: "Profile IMG")
        nameField.text = ""
        nameField.delegate = self
        profilePicture.layer.cornerRadius = 25
        numberBox.setShadow(view: numberBox, radius: 15, color: pinkColor, shadowRadius: 12, offHeight: 15, opacity: 0.6)
        navigationController?.interactivePopGestureRecognizer?.delegate = self as? UIGestureRecognizerDelegate
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        
        nameField.text = ""
        
        
        
        
       uploadBtn.layer.cornerRadius = 12
        
        

        self.hideKeyboard()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == nameField{
            textField.resignFirstResponder()
        }
       
        return true
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue{
            //self.view.frame.origin.y = 0
            let sizer = keyboardSize.height - 70
            let minus = self.view.frame.origin.y - (sizer)
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y = minus//minus//keyboardSize.height //- 100
                
            }
            
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
         if segue.identifier == "nextView2"{
             let dest = segue.destination as! PeriodQuestionsViewController
            
            dest.name.append(contentsOf: sender as! String)
            dest.nextInfo.append(contentsOf: info)
            
            dest.pic = profilePicture.image
            // dest.info.append(contentsOf: sender as! [String])
         }
     }
    
     @IBAction func nextBtn(_ sender: AnyObject?){
        guard let name = nameField.text, !name.isEmpty else {
           // show an alert
                 displayErrorMessage(message: "Please let us know what to call you.")
                 

                 
           return
        }
         
        if !isValidName(nameStr: name){
            displayErrorMessage(message: "Please only use letters for your name.")
        }
         
        performSegue(withIdentifier: "nextView2", sender: name)
         
     }
    
    
    
    func displayErrorMessage(message:String) {
        let alertView = UIAlertController(title: "Sorry!", message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
        }
        alertView.addAction(OKAction)
        if let presenter = alertView.popoverPresentationController {
            presenter.sourceView = self.view
            presenter.sourceRect = self.view.bounds
        }
        self.present(alertView, animated: true, completion:nil)
    }
    
    
    
    @IBAction func newImage(_ sender: Any){
        
        var config = YPImagePickerConfiguration()
        
        config.library.minNumberOfItems = 1
        
        config.library.maxNumberOfItems = 1
        config.library.onlySquare  = true
        config.library.mediaType = .photo
        config.startOnScreen = YPPickerScreen.photo
        config.screens = [.library]
        
        config.library.skipSelectionsGallery = false
        let picker = YPImagePicker(configuration: config)

        profilePicture.image = nil
        
        picker.didFinishPicking { [unowned picker] items, _ in
            if let photo = items.singlePhoto {
                
                self.profilePicture.image = photo.image
            }
            
            picker.dismiss(animated: true, completion: nil)
            if self.profilePicture.image == nil{
                self.profilePicture.image = UIImage(named: "Profile IMG")
            }
        }
        present(picker, animated: true, completion: nil)
        
        
    }
    
    
    func isValidName(nameStr:String) -> Bool {
        let nameRegEx = "[A-Za-z]{2,24}"

        let namePred = NSPredicate(format:"SELF MATCHES %@", nameRegEx)
        return namePred.evaluate(with: nameStr)
    }
    
    

}



extension ViewController: UIGestureRecognizerDelegate {
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return (navigationController?.viewControllers.count)! > 1 ? true : false
    }
}



