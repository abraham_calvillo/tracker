//
//  LogInViewController.swift
//  tracker
//
//  Created by Abraham Calvillo on 1/17/20.
//  Copyright © 2020 AbrahamCalvillo. All rights reserved.
//

import UIKit
import Parse
import AuthenticationServices


class AuthDelegate:NSObject, PFUserAuthenticationDelegate {
    func restoreAuthentication(withAuthData authData: [String : String]?) -> Bool {
        return true
    }
    
    func restoreAuthenticationWithAuthData(authData: [String : String]?) -> Bool {
        return true
    }
}

class LogInViewController: UIViewController, UITextFieldDelegate,ASAuthorizationControllerDelegate, ASAuthorizationControllerPresentationContextProviding {
    
    
    
    
    @IBOutlet fileprivate var username: UITextField!
    @IBOutlet fileprivate var password: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        username.setBottomBorder(color: .lightGray, opacity: 0.7)
        password.setBottomBorder(color: .lightGray, opacity: 0.7)
        
        username.delegate = self
        password.delegate = self
        
        username.text = ""
        password.text = ""
       /*
        if #available(iOS 13.0, *) {
            setupView()
        } */
        self.hideKeyboard()
    }
    /*
     MARK: Keyboard Stuff
     */
    
    @IBAction func pressedFB(_ sender: Any?){
        displayErrorMessage(message: "Still working on that! In the mean time, signup using your email. ")
    }
    
    
    @objc func keyboardWillShow(notification: NSNotification) {
        
        
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue{
            
            
            //self.view.frame.origin.y = 0
            let sizer = keyboardSize.height - 100
            let minus = self.view.frame.origin.y - (sizer - 70)
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y = minus//minus//keyboardSize.height //- 100
                
                
            }
            
            
        }
        
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        
        if self.view.frame.origin.y != 0 {
            
            self.view.frame.origin.y = 0
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == username && username.isFirstResponder {
            
            password.becomeFirstResponder()
        } else if textField == password {
            
            
            
            textField.resignFirstResponder()
        }
        return true
    }
    
    
    
    /*
     // MARK: - log in 
     */
    
    
    func loadHomeScreen(){
//        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        let loggedInViewController = storyBoard.instantiateViewController(withIdentifier: "Home") as! ViewController
//        self.present(loggedInViewController, animated: false, completion: nil)
       

         let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
               //let vc = storyBoard.instantiateViewController(withIdentifier: "Home") as! ViewController
               let vc = storyBoard.instantiateViewController(withIdentifier: "tabBar")
               let childNavigation = UINavigationController(rootViewController: vc)
               vc.modalPresentationStyle = .fullScreen
               childNavigation.willMove(toParent: self)
                childNavigation.isNavigationBarHidden = true
               addChild(childNavigation)
               childNavigation.view.frame = view.frame
               view.addSubview(childNavigation.view)
        

               childNavigation.didMove(toParent: self)
    }
    
    @IBAction func signIn(_ sender: UIButton) {
        let sv = UIViewController.displaySpinner(onView: self.view)
        PFUser.logInWithUsername(inBackground: username.text!, password: password.text!) { (user, error) in
            UIViewController.removeSpinner(spinner: sv)
            if user != nil {
                self.loadHomeScreen()
                
            }else{
                if let descrip = error?.localizedDescription{
                    self.displayErrorMessage(message: (descrip))
                }
            }
        }
        
        

    }
    
 
    
    func displayErrorMessage(message:String) {
        let alertView = UIAlertController(title: "Sorry!", message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
        }
        alertView.addAction(OKAction)
        if let presenter = alertView.popoverPresentationController {
            presenter.sourceView = self.view
            presenter.sourceRect = self.view.bounds
        }
        self.present(alertView, animated: true, completion:nil)
    }
    
    
    // MARK: - FORGOT PASSWORD
    @IBAction func forgotPasswButt(_ sender: AnyObject) {
        let alert = UIAlertController(title: "Full Moon",
                                      message: "Type the email address you've used to sign up.",
                                      preferredStyle: .alert)
        
        let ok = UIAlertAction(title: "Reset Password", style: .default, handler: { (action) -> Void in
            // TextField
            let textField = alert.textFields!.first!
            let txtStr = textField.text!
            
            PFUser.requestPasswordResetForEmail(inBackground: txtStr, block: { (succ, error) in
                if error == nil {
                    self.simpleAlert("You will receive an email shortly with a link to reset your password")
                }})
        })
        
        
        
        
        // Cancel button
        let cancel = UIAlertAction(title: "Cancel", style: .destructive, handler: { (action) -> Void in })
        
        // Add textField
        alert.addTextField { (textField: UITextField) in
            textField.keyboardAppearance = .dark
            textField.keyboardType = .default
        }
        
        alert.addAction(ok)
        alert.addAction(cancel)
        present(alert, animated: true, completion: nil)
    }
    
    
    func simpleAlert(_ mess:String) {
        let alert = UIAlertController(title: "Full Moon", message: mess, preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default, handler: { (action) -> Void in
            self.navigationController?.popViewController(animated: true)
        })
        alert.addAction(ok)
        present(alert, animated: true, completion: nil)
    }
    
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
            switch authorization.credential {
            case let credentials as ASAuthorizationAppleIDCredential:
                print("AUTHORIZED: \(credentials.fullName?.givenName ?? "")")
                
                let token = credentials.identityToken!
                let tokenString = String(data: token, encoding: .utf8)!
                let user = credentials.user
                
                print("TOKEN: \(tokenString)")
                print("USER: \(user)")
                
                PFUser.register(AuthDelegate(), forAuthType: "apple")
                
                PFUser.logInWithAuthType(inBackground: "apple", authData: ["token":tokenString, "id": user]).continueWith { task -> Any? in
    //                guard task.error == nil, let _ = task.result else {
    //                    print("TASK: \(String(describing: task.result?.email!))")
    //                    return task
    //                }
    //
    //                if ((task.error) != nil){
    //                    print("ERROR: \(task.error?.localizedDescription)")
    //                }
    //                return task
                    
                    if let userObject = task.result {
                        // Fill userObject (which is PFUser) by profile data, like:
                        //userObject.email = user.profile.email
                        //userObject.password = UUID().uuidString
                        //userObject["firstName"] = user.profile.givenName
                        //userObject["lastName"] = user.profile.familyName
                        self.loadHomeScreen()
                        print("LOGGED IN PARSE")
                    } else {
                        // Failed to log in.
                        print("ERROR LOGGING IN IN PARSE: \(task.error?.localizedDescription)")
                    }
                    return nil
                }
                
                break
            default:
                break
            }
        }
        
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
            print("ERROR: \(error)")
        }
        
    @available(iOS 13.0, *)
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
            return view.window!
        }
    
    @available(iOS 13.0, *)
    func setupView(){
        let signInWithAppleButton = ASAuthorizationAppleIDButton()
        signInWithAppleButton.translatesAutoresizingMaskIntoConstraints = false
        signInWithAppleButton.addTarget(self, action: #selector(didClickSignInWithAppleButton), for: .touchUpInside)
        
        view.addSubview(signInWithAppleButton)
        NSLayoutConstraint.activate([
            signInWithAppleButton.centerYAnchor.constraint(equalTo: view.centerYAnchor , constant: 30),
            signInWithAppleButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 30),
            signInWithAppleButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -30)
        ])
    }
    
    @available(iOS 13.0, *)
    @objc
    func didClickSignInWithAppleButton(){
        let provider = ASAuthorizationAppleIDProvider()
        let request = provider.createRequest()
        request.requestedScopes = [.fullName, .email]
        
        let controller = ASAuthorizationController(authorizationRequests: [request])
        
        controller.delegate = self
        controller.presentationContextProvider = self
        
        controller.performRequests()
    }
}




