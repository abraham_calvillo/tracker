//
//  FinalSignUpView.swift
//  tracker
//
//  Created by Abraham Calvillo on 11/24/19.
//  Copyright © 2019 AbrahamCalvillo. All rights reserved.
//

import UIKit
import Parse

class FinalSignUpView: UIViewController {
    var avgFlow: Int!

    @IBOutlet weak var remindersBtn: UIButton!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var dashboardBtn: UIButton!
    
    @IBOutlet weak var profileHolder: UIView!
    @IBOutlet weak var name: UILabel!
    var prof: UIImage!

    var nme: String!
     var modelData: [modelNumber]!
    let customWidth = 60
       let customHeight = 60
       
       @IBOutlet weak var holder: UIView!
    @IBOutlet weak var numberModelPicker: UIPickerView!
       var dataModelPicker: FlowLevelViewController!
       var rotationAng: CGFloat!
    
    override func viewDidLayoutSubviews() {
           super.viewDidLayoutSubviews()
           
           
           
           
           for i in [1,2]{
               numberModelPicker.subviews[i].isHidden = true
           }
       }
       
 
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
        let  user = PFUser.current()!
        
       
        numberModelPicker.delegate = self
        
        //profileImage.layer.cornerRadius = 12
        remindersBtn.layer.cornerRadius = 12
        dashboardBtn.layer.borderWidth = 1
        dashboardBtn.layer.borderColor = UIColor.lightGray.cgColor
        dashboardBtn.layer.cornerRadius = 12
        
        profileHolder.setShadow(view: profileHolder, radius: 15, color: pinkColor, shadowRadius: 12, offHeight: 15, opacity: 0.6)
        
        
            profileImage.image = prof
        name.text = nme
        
        rotationAng = -90 * (.pi/180)
        let originalY = numberModelPicker.frame.origin.y
        let originalX = numberModelPicker.frame.origin.x
        
        dataModelPicker = FlowLevelViewController()
        dataModelPicker.modelData = numbers.getNumbers()

        
        numberModelPicker.transform = CGAffineTransform(rotationAngle: rotationAng)
        numberModelPicker.frame = CGRect(x: originalX , y: originalY , width: holder.frame.width , height: 60)
        
        numberModelPicker.delegate = dataModelPicker!
        numberModelPicker.dataSource = dataModelPicker!
        
        numberModelPicker.selectRow(4, inComponent: 0, animated: true)
       
        

    }
    
    
    @IBAction func goToHome(_ sender: Any?){
        
//    let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "tabBar") as! BubbleTabBarController
//    //homeVC.modalTransitionStyle = .crossDissolve
//    present(homeVC, animated: true, completion: nil)
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let loggedInViewController = storyBoard.instantiateViewController(withIdentifier: "tabBar") as! BubbleTabBarController
        self.modalPresentationStyle = .fullScreen
            self.present(loggedInViewController, animated: true, completion: nil)
        

        
    }
    
    
    


}


extension FinalSignUpView: UIPickerViewDataSource{
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return modelData.count
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
}

extension FinalSignUpView: UIPickerViewDelegate{
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return String(modelData[row].days)
    }
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return CGFloat(customHeight)
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let view = UIView()
        
        
        let backgrouund = UIView(frame: CGRect(x: 0, y: 0, width: customWidth  , height: customHeight))
        backgrouund.backgroundColor = #colorLiteral(red: 0.9019607843, green: 0.9725490196, blue: 1, alpha: 1)
        backgrouund.layer.cornerRadius = 12
        view.addSubview(backgrouund)
        let middleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: customWidth, height: customHeight))
        middleLabel.text = String(modelData[row].days)
        middleLabel.textColor = UIColor.black
        middleLabel.textAlignment = .center
        middleLabel.font = UIFont(name: "SofiaPro-Light", size: 16)
        view.addSubview(middleLabel)
        view.transform = CGAffineTransform(rotationAngle: (90 * (.pi/180)))
        
        return view
    }
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        avgFlow = modelData[row].days
        print(modelData[row].days)
        
    }
    
    
}
