//
//  NumberModelPicker.swift
//  tracker
//
//  Created by Abraham Calvillo on 11/21/19.
//  Copyright © 2019 AbrahamCalvillo. All rights reserved.
//

import UIKit

class NumberModelPicker: UIPickerView {

    var modelData: [modelNumber]!
    let customWidth = 60
    let customHeight = 60
   
    
}

extension NumberModelPicker: UIPickerViewDataSource{
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
       return modelData.count
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
}

extension NumberModelPicker: UIPickerViewDelegate{
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return String(modelData[row].days)
    }
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return CGFloat(customHeight)
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let view = UIView()
        
        
        let backgrouund = UIView(frame: CGRect(x: 0, y: 0, width: customWidth  , height: customHeight))
        backgrouund.backgroundColor = #colorLiteral(red: 0.9019607843, green: 0.9725490196, blue: 1, alpha: 1)
        backgrouund.layer.cornerRadius = 12
        view.addSubview(backgrouund)
        let middleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: customWidth, height: customHeight))
        middleLabel.text = String(modelData[row].days)
        middleLabel.textColor = UIColor.black
        middleLabel.textAlignment = .center
        middleLabel.font = UIFont(name: "SofiaPro-Light", size: 16)
        view.addSubview(middleLabel)
        view.transform = CGAffineTransform(rotationAngle: (90 * (.pi/180)))
        
        return view
    }
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        
    }
    
    
}

