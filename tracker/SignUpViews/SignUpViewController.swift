//
//  SignUpViewController.swift
//  tracker
//
//  Created by Abraham Calvillo on 9/18/19.
//  Copyright © 2019 AbrahamCalvillo. All rights reserved.
//

import UIKit
//import Pastel
import Parse


class SignUpViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var userNA: UIImageView!
    @IBOutlet fileprivate weak var emailTextField: UITextField!
    
    @IBOutlet fileprivate weak var passwordTextfield: UITextField!
    @IBOutlet fileprivate weak var passwordConfirmTextField: UITextField!
    
    @IBOutlet weak var fbLogin: UIButton!
    @IBOutlet weak var googleLogin: UIButton!
    @IBOutlet weak var nextPink: UIButton!
    
    
    override func viewWillAppear(_ animated: Bool) {
        let user = PFUser.current()
        if user != nil {
            
            if(user!["themeColor"] as? String == "pinkColor")   {
                themeColor = pinkColor
            }
            else if(user!["themeColor"] as? String == "blueColor")   {
                themeColor = blueColor
                
            }
            else if(user!["themeColor"] as? String == "maroonColor"){
                themeColor = maroonColor
            }
            else if(user!["themeColor"] as? String == "purpColor"){
                themeColor = purpleColor
            }
            else if(user!["themeColor"] as? String == "greenColor"){
                themeColor = greenColor
            }
            loadHomeScreen()
        }
    }
    
    
    func loadHomeScreen(){
//        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        let vc = storyBoard.instantiateViewController(withIdentifier: "Home") as! ViewController
//
//        self.present(vc, animated: false, completion: nil)
        
        
        //self.present(navController, animated:true, completion: nil)
//        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        //let vc = storyBoard.instantiateViewController(withIdentifier: "Home") as! ViewController
//        let vc = storyBoard.instantiateViewController(withIdentifier: "tabBar")
//        let childNavigation = UINavigationController(rootViewController: vc)
//
//        childNavigation.willMove(toParent: self)
//        addChild(childNavigation)
//        childNavigation.view.frame = view.frame
//        view.addSubview(childNavigation.view)
//        childNavigation.didMove(toParent: self)
//
        
        UserDefaults.standard.set(false, forKey: "isUserLoggedIn")
                  UserDefaults.standard.synchronize()
                  
                  let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "tabBar") as! BubbleTabBarController
                  
                  let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                  
                  appDel.window?.rootViewController = loginVC
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        self.passwordTextfield.isSecureTextEntry = false
        self.passwordConfirmTextField.isSecureTextEntry = false
        
        emailTextField.autocorrectionType = .no
        passwordTextfield.autocorrectionType = .no
        passwordConfirmTextField.autocorrectionType = .no
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        
        emailTextField.text = ""
        passwordTextfield.text = ""
        passwordConfirmTextField.text = ""
        
        
        
        definesPresentationContext = true
        
        emailTextField.delegate = self
        passwordTextfield.delegate = self
        passwordConfirmTextField.delegate = self
        
        
        emailTextField.setBottomBorder(color: .lightGray, opacity: 0.7)
        passwordTextfield.setBottomBorder(color: .lightGray, opacity: 0.7)
        passwordConfirmTextField.setBottomBorder(color: .lightGray, opacity: 0.7)
        
        userNA.setShadow(view: userNA, radius: 5, color: .lightGray, shadowRadius: 12, offHeight: 15, opacity: 0.6)
        
        
        emailTextField.addTarget(self, action: #selector(SignUpViewController.selectedField(_:)), for: UIControl.Event.editingChanged)
        emailTextField.addTarget(self, action: #selector(SignUpViewController.deselectField(_:)), for: UIControl.Event.editingDidEnd )
        
        passwordTextfield.addTarget(self, action: #selector(SignUpViewController.selectedField(_:)), for: UIControl.Event.editingChanged)
        passwordTextfield.addTarget(self, action: #selector(SignUpViewController.deselectField(_:)), for: UIControl.Event.editingDidEnd )
        
        passwordConfirmTextField.addTarget(self, action: #selector(SignUpViewController.selectedField(_:)), for: UIControl.Event.editingChanged)
        passwordConfirmTextField.addTarget(self, action: #selector(SignUpViewController.deselectField(_:)), for: UIControl.Event.editingDidEnd )
        
        
        
        
        
        self.hideKeyboard()
    }
    
    
    
    @objc func selectedField(_ sender: UITextField){
        sender.setBottomBorder(color: pinkUnderline, opacity: 1.0)
    }
    
    @objc func deselectField (_ sender: UITextField){
        if(sender.text!.isEmpty){
            sender.setBottomBorder(color: .lightGray, opacity: 0.7)
        }
    }
    
    
    @IBAction func pressedFB(_ sender: Any?){
        displayErrorMessage(message: "Still working on that! In the mean time, signup using your email. ")
    }
    
    
    
    
    @objc func keyboardWillShow(notification: NSNotification) {
        
        
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue{
            
            
            
            
            //self.view.frame.origin.y = 0
            let sizer = keyboardSize.height - 100
            let minus = self.view.frame.origin.y - (sizer - 70)
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y = minus//minus//keyboardSize.height //- 100
                
                
            }
            
            
        }
        
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        
        if self.view.frame.origin.y != 0 {
            
            self.view.frame.origin.y = 0
        }
    }
    
    
    
    /*
     This piece of code below is a "hacky" way of removing autocomplete feature and also allowing secure text
     entry on the password fields since iOS 12 has a bug when using secure text entry. Only solution I can fin
     will revisit once this bug is fixed but should work now!
     */
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if ( textField == self.passwordTextfield  || textField == self.passwordConfirmTextField && !self.passwordTextfield.isSecureTextEntry || !self.passwordConfirmTextField.isSecureTextEntry){
            self.passwordConfirmTextField.isSecureTextEntry = true
            self.passwordTextfield.isSecureTextEntry = true
        }
        
        return true
    }
    
    
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == emailTextField && emailTextField.isFirstResponder {
            
            passwordTextfield.becomeFirstResponder()
        } else if textField == passwordTextfield {
            
            passwordConfirmTextField.becomeFirstResponder()
        } else if textField == passwordConfirmTextField {
            
            textField.resignFirstResponder()
        }
        return true
    }
    
    
    
    func displayErrorMessage(message:String) {
        let alertView = UIAlertController(title: "Sorry!", message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
        }
        alertView.addAction(OKAction)
        if let presenter = alertView.popoverPresentationController {
            presenter.sourceView = self.view
            presenter.sourceRect = self.view.bounds
        }
        self.present(alertView, animated: true, completion:nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "nextView"{
            let dest = segue.destination as! PeronalInfoViewController
            dest.info.append(contentsOf: sender as! [String])
            
            
        }
        if segue.identifier == "welcome"{
        let dest = segue.destination as! LogInViewController
        }
    }
    
    @IBAction func nextPink(_ sender: AnyObject?){
        guard let email = emailTextField.text, !email.isEmpty,
            let password = passwordTextfield.text, !password.isEmpty else {
                // show an alert
                displayErrorMessage(message: "Please fill out all fields.")
                
                
                return
        }
        
        if passwordTextfield.text != passwordConfirmTextField.text{
            displayErrorMessage(message: "The passwords do not match.")
            return
        }
        
        if !isValidEmail(emailStr: email){
            displayErrorMessage(message: "Please use a valid email.")
            return
        }
        if passwordTextfield.text!.count <= 7 {
            displayErrorMessage(message: "Password must be more that 8 characters.")
            return
        }
        performSegue(withIdentifier: "nextView", sender: [email, password])
        
    }
    
    @IBAction func unwindToThisViewController(segue: UIStoryboardSegue) {
    }
    
    func isValidEmail(emailStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: emailStr)
    }
    
    
    /*
     MARK: - Log in button
     */
    
    
    @IBAction func loginBtn(){

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "loginView")
        self.present(controller, animated: true, completion: nil)

    }
    
    
}






