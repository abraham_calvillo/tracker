//
//  numbers.swift
//  tracker
//
//  Created by Abraham Calvillo on 11/21/19.
//  Copyright © 2019 AbrahamCalvillo. All rights reserved.
//

import Foundation


class numbers {
    class func getNumbers() -> [modelNumber]{
        var numbers = [modelNumber]()
        for i in 1...31 {
            numbers.append(modelNumber(day: i))
        }
        return numbers
    }
}
