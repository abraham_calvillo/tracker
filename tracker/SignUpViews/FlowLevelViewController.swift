//
//  PeriodQContViewController.swift
//  tracker
//
//  Created by Abraham Calvillo on 11/21/19.
//  Copyright © 2019 AbrahamCalvillo. All rights reserved.
//

import UIKit
import Parse

class FlowLevelViewController: UIViewController{
    let customWidth = 60
    let customHeight = 60
    
    var modelData: [modelNumber]!
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet var btns: [UIButton]!
    
    @IBOutlet weak var numberBox: UILabel!
    @IBOutlet var bars: [UIView]!
    
    var lastInfo = [String]()
    var nameN = ""
    var profile: UIImage!
    var dateDiff: Int!
    var selectedNuber: Int!
    
    var avgFlow: Int!
    
    var dates = [Date]()
    
    var flowLevel = "nil"
    var first: Date!
    var last: Date!
    
    
    @IBOutlet weak var holder: UIView!
    weak var nickName: PeronalInfoViewController!
    
    
    
    @IBOutlet weak var numberModelPicker: UIPickerView!
    var dataModelPicker: FlowLevelViewController!
    var rotationAng: CGFloat!
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        
        
        
        for i in [1,2]{
            numberModelPicker.subviews[i].isHidden = true
        }
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        numberBox.setShadow(view: numberBox, radius: 15, color: pinkColor, shadowRadius: 12, offHeight: 15, opacity: 0.6)
        
        numberModelPicker.delegate = self
        
        bars.forEach { bar in
            bar.layer.cornerRadius = 2
        }
        
        rotationAng = -90 * (.pi/180)
        let originalY = numberModelPicker.frame.origin.y
        let originalX = numberModelPicker.frame.origin.x
        
        dataModelPicker = FlowLevelViewController()
        dataModelPicker.modelData = numbers.getNumbers()
        
        numberModelPicker.transform = CGAffineTransform(rotationAngle: rotationAng)
        numberModelPicker.frame = CGRect(x: originalX , y: originalY , width: holder.frame.width , height: 60)
        
        numberModelPicker.delegate = dataModelPicker!
        numberModelPicker.dataSource = dataModelPicker!
        
        numberModelPicker.selectRow(4, inComponent: 0, animated: true)
        
        // print(numberModelPicker.selectedRow(inComponent: 0))
        
    }
    
    
    @IBAction func signUp(_ sender: AnyObject?) {
        let user = PFUser()
        
        
        user.username = lastInfo[0]
        user.email = lastInfo[0]
        user.password = lastInfo[1]
        
        
        let sv = UIViewController.displaySpinner(onView: self.view)
        user.signUpInBackground { (success, error) in
            UIViewController.removeSpinner(spinner: sv)
            
            if (error != nil){
                if let descrip = error?.localizedDescription{
                                   self.displayErrorMessage(message: descrip + "Username or Email are already being used." )
                               }
                               
            }else if success{
               
                self.updateCurrentUserProfilePicture(image: self.profile)
                self.loadHomeScreen()
                
                
            }
        }
    }
    
    
    
    
    func updateCurrentUserProfilePicture(image: UIImage) {
        let userToUpdate = PFUser.current()!
        
        // Save Avatar
        if self.profile != nil {
            let imageData = self.profile.jpegData(compressionQuality: 0.5)
            let imageFile = PFFileObject(name:"avatar.jpg", data:imageData!)
            userToUpdate["profileImg"] = imageFile
        }
        userToUpdate["usersName"] = nameN
        
        
        userToUpdate["avgDaysOfFlow"] = numberModelPicker.selectedRow(inComponent: 0) + 1
        userToUpdate["flowType"] = flowLevel
        userToUpdate["cycleEnd"] = dates[1]
        userToUpdate["cycleStart"] = dates[0]
        userToUpdate["themeColor"] = "pinkColor"
        
        // Saving block
        userToUpdate.saveInBackground(block: { (succ, error) in
            if error == nil {
                //print("Your Profile has been updated!")
            } else {
                //print("Failed")
                
            }})
    }
    
    
    func loadHomeScreen(){
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let loggedInViewController = storyBoard.instantiateViewController(withIdentifier: "tabBar") as! BubbleTabBarController
//        loggedInViewController.prof = profile
//        loggedInViewController.nme = nameN
        self.present(loggedInViewController, animated: true, completion: nil)
    }
    func displayErrorMessage(message:String) {
        let alertView = UIAlertController(title: "Error!", message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
        }
        alertView.addAction(OKAction)
        if let presenter = alertView.popoverPresentationController {
            presenter.sourceView = self.view
            presenter.sourceRect = self.view.bounds
        }
        self.present(alertView, animated: true, completion:nil)
    }
    
    
    @IBAction func buttonPressed(_ sender: UIButton) {
        btns.forEach { $0.isSelected = false
            
            //$0.backgroundColor = .white
            bars[$0.tag].backgroundColor = .lightGray
            
            
        }
        let flowType = String(btns[sender.tag].restorationIdentifier!)
        switch flowType {
        case "realLow" : flowLevel = "Light Flow"
        case "lFull-Moon" : flowLevel = "Regular Flow"
        case "medium" : flowLevel = "Medium Flow"
        case "high" : flowLevel = "Heavy Flow"
        default: flowLevel = "Medium"
        }
        //
        //        flowLevel = String(btns[sender.tag].restorationIdentifier!)
        
        //print(flowLevel)
        //btns[sender.tag].backgroundColor = pinkColor
        bars[sender.tag].backgroundColor = pinkColor
        btns[sender.tag].isSelected = true
    }
    
    
    
    
}



extension FlowLevelViewController: UIPickerViewDataSource{
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return modelData.count
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
}

extension FlowLevelViewController: UIPickerViewDelegate{
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return String(modelData[row].days)
    }
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return CGFloat(customHeight)
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let view = UIView()
        
        
        let backgrouund = UIView(frame: CGRect(x: 0, y: 0, width: customWidth  , height: customHeight))
        backgrouund.backgroundColor = #colorLiteral(red: 0.9019607843, green: 0.9725490196, blue: 1, alpha: 1)
        backgrouund.layer.cornerRadius = 12
        view.addSubview(backgrouund)
        let middleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: customWidth, height: customHeight))
        middleLabel.text = String(modelData[row].days)
        middleLabel.textColor = UIColor.black
        middleLabel.textAlignment = .center
        middleLabel.font = UIFont(name: "SofiaPro-Light", size: 16)
        view.addSubview(middleLabel)
        view.transform = CGAffineTransform(rotationAngle: (90 * (.pi/180)))
        
        return view
    }
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        avgFlow = modelData[row].days
        //print(modelData[row].days)
        
    }
    
    
}
