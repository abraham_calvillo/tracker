//
//  ListDetails.swift
//  tracker
//
//  Created by Abraham Calvillo on 3/17/20.
//  Copyright © 2020 AbrahamCalvillo. All rights reserved.
//

import UIKit



class ListDetails: UIViewController {
    
    

    @IBOutlet weak var articleTitle: UILabel!
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var articleImg: UIImageView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var container: UIScrollView!
    var textHolder: String!
    var picHolder: UIImage!
    var authorHolder: String!
    var titleHolder: String!
    @IBOutlet weak var articleText: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
navigationController?.interactivePopGestureRecognizer?.delegate = self as? UIGestureRecognizerDelegate
        self.navigationController?.isNavigationBarHidden = true

        setupView()
        
        
    }
    
    func setupView(){
        
        
        let sv = UIViewController.displaySpinner(onView: self.view)
        articleText.setHTMLFromString(htmlText: textHolder)
        articleText.sizeToFit()
        articleImg.image = picHolder
        authorLabel.text = authorHolder
        articleTitle.text = titleHolder
        articleTitle.font = UIFont(name: "SofiaPro-Medium", size: 25)
        bottomView.frame.origin.y = articleText.frame.size.height + articleText.frame.origin.y
        container.contentSize = CGSize(width: container.frame.size.width,
        height: articleText.frame.size.height + articleText.frame.origin.y)
        sleep(UInt32(0.5))
            UIViewController.removeSpinner(spinner: sv)
        

    }
    

}
